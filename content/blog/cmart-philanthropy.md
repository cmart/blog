---
title: "cmart Philanthropy"
date: 2024-02-25T19:55:16-07:00
tags: []
draft: false
---

I sponsor development of technologies that answer to their users' own interests, rather than the interests of megacorps (who harvest users' data and attention, extract rents, impose lock-in, and so on). I particularly support indie developers who are heading upstream of their own economic incentives. I also support some creative work like music and blogs.

I should do more of this, as should everyone who benefits from these works. If you can afford streaming services or cafe drinks, you can afford a few bucks to make the digital world a richer place. Patronage is not a perfect economic model, but it helps independent creators build their careers. (See, also, [Quality software deserves your hard‑earned cash](https://stephango.com/quality-software).) I have a bias toward _recurring over one-time donations_, especially when our continued benefit from the project requires ongoing maintenance work.

For now, "philanthropy" is tongue-in-cheek, because you can total everything below and it doesn't even add up to a car payment. But I'm sharing the list to nudge myself to do more, and maybe inspire others to think philanthropically, too.

## Current Portfolio

[GrapheneOS](https://grapheneos.org) / Daniel Micay: **$600/year** via [GitHub Sponsors](https://github.com/thestinger) since Apr 2022. GrapheneOS is an alternative operating system for Google Pixel phones and tablets. It is the open-source core of Android, without any of the Google services or surveillance, plus many security hardening enhancements, plus very slick installation and over-the-air update mechanisms. I believe GrapheneOS is the pareto frontier of privacy, security, and autonomy on a mobile device. Apple will sell you semi-privacy as a luxury good, but Daniel Micay and his contributors just give it to you, while respecting your freedom to install open-source software from wherever you wish.

[Internet Archive](https://archive.org): **$120/year** via itself since Jul 2023. archive.org is our collective digital memory. Critical infrastructure for an information-based society. Ambitious and underappreciated mission.

[Path Less Pedaled](https://m.youtube.com/channel/UCaThRBMEp21yRK4seqq3-Sw): **$75.61/year** via [Patreon](https://www.patreon.com/PathLessPedaled) since Dec 2023. Russ Roca will help you get more fun, utility, and comfort out of cycling. I believe more people in the USA would ride bikes if a major bicycle manufacturer put Russ in charge of product development and/or marketing. The unfortunate YouTube clickbait is mostly balanced out by a big dose of hacker/maker spirit.

[Joplin](https://joplinapp.org) / Laurent Cozic: **$60/year** via [Patreon](https://www.patreon.com/joplin) since Feb 2021. Joplin is a [Markdown](https://www.markdownguide.org)-based note-taking app with several client options. It provides end-to-end encrypted multi-device sync using commodity cloud storage. If you're using something like Evernote or Simplenote, but don't like that the company behind it can look at all your notes, then Joplin may be for you.

[OpenSnitch](https://github.com/evilsocket/opensnitch) / maintainer [Gustavo Iñiguez Goia](https://github.com/gustavo-iniguez-goya): **$60/year** via [GitHub Sponsors](https://github.com/sponsors/gustavo-iniguez-goya). OpenSnitch is an interactive firewall for outbound network connections that applications on your copmuter make. It prevents software from "phoning home" or uploading telemetry to its developer without your consent. I discovered a [privacy concern](https://gitlab.gnome.org/GNOME/gnome-software/-/issues/2556) in GNOME Software just a few minutes after installing OpenSnitch.

[Codeberg](https://codeberg.org/): **$52/year** via [Liberapay](https://liberapay.com/codeberg/) since Jan 2024. Somehow, the center of the open-source software universe is a closed-source engagement-hacked social networking platform owned by Microsoft. Fortunately, some folks around the edges are devloping third spaces for repository hosting and project management. Codeberg, based in Germany, is doing this with community governance. They also develop [Forgejo](https://forgejo.org), the underlying software platform. (See also: Sourcehut and Phabricator.)

[Matrix Foundation](https://matrix.org): **€52/year** via [Liberapay](https://liberapay.com/matrixdotorg) since Jun 2023. Somehow, thousands of open public communities have ended up on a proprietary chat platform owned by Salesforce. Salesforce has cranked up the retrograde amnesia in hopes of monetizing these communities, most of whom could never afford the sticker price of a paid plan. I believe Matrix and [Element](https://element.io) is what many of them should use instead. Matrix is the federated communication protocol with not-for-profit stewardship, while Element is one of several client applications for it.

[NewPipe](https://newpipe.net): **€52/year** via [Liberapay](https://liberapay.com/TeamNewPipe/) since Jan 2024. NewPipe is a lightweight and privacy-friendly YouTube client for Android. It makes YouTube (somewhat!) less toxic to consume. Watch videos with no ads, play music with the app in the background, and download things to play offline.

[Liberapay](https://liberapay.com/Liberapay/): **$13/year** via [itself](https://liberapay.com/Liberapay) since Jan 2024. Liberapay is an open-source donations platform that enables several of my other recurring donations. It's financial infrastructure for developing humane technology. I prefer it over Patreon or GitHub Sponsors.

[k9mail](https://k9mail.app/): **$13/year** via [Liberapay](https://liberapay.com/k9mail/) since Feb 2021. Fast native email (POP/IMAP) client for Android.

[DAVx⁵](https://www.davx5.com/) / [ICSx⁵](https://icsx5.bitfire.at/): **€10/year** via [Donorbox](https://donorbox.org/davx5). These Android apps let you synchronize your self-hosted calendars, contacts, and todo lists with other devices.

[Miryoku](https://github.com/manna-harbour/miryoku): **$50 (one-time)** via [GitHub Sponsors](https://github.com/manna-harbour). Intuitive ergonomic keyboard layout, uses only 36 keys, really deserves its own blog post.

[ZMK](https://zmk.dev) / Pete Johanson: **$15.10 (matched by Typeractive, one-time)** via [Typeractive](https://typeractive.xyz). Open-source, very hackable firmware for wireless keyboards. I should make this one recurring.

## Emeritus (supported in the past)

[Crickles](https://crickles.casa/home/): **£45/year** via [Wordpress](https://wordpress.com) from Feb 2022 to Feb 2024. Cool project and interesting site to read, but I didn't use it much, and there isn't a lot of insight you can gain from heart rate data.

[savagegeese](https://www.youtube.com/channel/UCgUvk6jVaf-1uKOqG8XNcaQ). This is a YouTube channel of vehicle reviews and automotive technology, but underlying all of it is Mr. Goose and Jack Singapore speaking truth to power in the automotive industry. I stopped donating mostly because I believe humanity needs a future with less driving and fewer cars.

[AvE](https://www.youtube.com/user/arduinoversusevil/videos), a very Canadian enginerd and Renaissance man. I have learned more about metallurgy, machining, and welding from AvE than from the rest of the internet combined. The signal-to-noise ratio varies a lot, but at his best, AvE is a paragon of curiosity with a spirit of exploration. I stopped donating because over time, the content got less educational and more click-baity.

[JAWWS](https://jawws.org/), Journal of Accessible and Well-Written Science. Scientific journal articles tend to be unnecessarily difficult to read, especially for readers who are less familiar with the authors' field of research. This limits public understanding of new science and its potential to benefit the world. Étienne Fortier-Dubois is (was?) trying to fix that with JAWWS, to the extent that it can be fixed given competing and even countervailing incentives. The project seems mostly abandoned, so I stopped donating.

## Things I Buy

This isn't sponsorship per se, these are products or services that cost money. But I pay for them in hopes that they'll stick around for a while and not sell out. :)

[Kagi](https://kagi.com) since 2024. "Wait, a search engine that requires a paid subscription to use??" Yes. The money means that they work for _you_ the user, not for advertisers trying to redirect your attention. Kagi emphasizes non-commercial and [Small Web](https://blog.kagi.com/small-web) search results -- web sites owned by regular people like you and me. So, Kagi results feel _learning-focused_ in an age when Google results feel _buying-focused_. You can block domains that you don't want to see results from (bye, Pinterest and Medium). Kagi also offers an AI assistant (a wrapper around GPT-4), so I happily cancelled my ChatGPT Plus subscription. The LLM integration is mercifully optional, not [all over everything](https://www.instagram.com/reel/C6glvr7NKDS).

[Fastmail](https://www.fastmail.com) since 2023. For 8 years I self-hosted my own email service, from bare metal to DKIM. It mostly worked fine, but I eventually gave up. It's a bunch of work to get Google to reliably accept messages you send to Gmail users. Now I happily pay Fastmail to host my email. The service and web app are great. You're the customer, not the product. Yes, they can read my messages, but I've also given up on trying to use email as a secure way to communicate. (Shout out to the ~3 people who have ever corresponded with me via PGP...)

[Framework](https://frame.work) computers since 2023. Framework laptops cost more than equivalent-spec Thinkpads, but they're explicitly easy to service, repair, and upgrade (even across 4+ CPU generations). You can buy every part as a spare, including main boards. The expansion card system lets you customize your ports, and it offloads the mechanical wear of plugging/unplugging onto easily-replaceable dongles.

## Music

I can't be bothered to list everything I've bought on Bandcamp, so go look at my [Bandcamp profile](https://bandcamp.com/cmart42069). But two highlights:

[Neil Cicierega](http://neilcic.com), who makes music as [Lemon Demon](https://lemondemon.bandcamp.com). He also created the amazing [Mouth tetralogy](https://en.wikipedia.org/wiki/Neil_Cicierega#The_Mouth_mashup_albums_(2014%E2%80%93present)) of mash-up albums which he doesn't charge money for, so if you enjoy those you should buy Spirit Phone and Nature Tapes. Maybe proceed carefully if you're prone to hypomania.

[King Gizzard and the Lizard Wizard](https://kinggizzard.bandcamp.com). They make proggy, melodic, catchy music. They created 5 albums in one year. Some of it is in [24TET](https://en.wikipedia.org/wiki/24_TET), which will be a whole new tuning system for most people who read this. Imagine you had only ever seen blue and green, and someone shows you teal for the first time.
