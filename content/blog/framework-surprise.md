---
title: "Framework 13: Things that Surprised Me"
date: 2023-09-02T15:18:36-07:00
tags: []
draft: false
---


If you don't know what a [Framework laptop](https://frame.work) is, go read [someone](https://www.theverge.com/23725039/framework-laptop-13-2023-intel-review) [else's](https://www.tomshardware.com/reviews/framework-laptop-13-intel-2023) [review](https://www.ifixit.com/News/51614/framework-laptop-teardown-10-10-but-is-it-perfect) first. This ain't the best introduction, but it _will_ be helpful if you are nitpicky, migrating from a Thinkpad, and/or running Linux. (I also posted this on the [Framework Discourse](https://community.frame.work/t/framework-13-things-that-surprised-me/36235).)

I am (approximately) a software and cloud infrastructure engineer. For about 8 years I've used Linux on Thinkpads, mostly [Debian](https://en.m.wikipedia.org/wiki/Debian) with the default [GNOME](https://en.m.wikipedia.org/wiki/GNOME) desktop environment. For the past three years I've also used the wonderful [PaperWM](https://github.com/paperwm/PaperWM) GNOME extension.  I've had my new Framework 13 (13th gen Intel CPU) for a couple of weeks. It's replacing a Thinkpad T480, which I previously wrote about [here](https://old.reddit.com/r/thinkpad/comments/jreqej/new_t480_impressions_upgrading_from_t450s). (Before that I had a T450s, and before that I had the _very first_ unibody Macbook.)

So, what has surprised me about the Framework?

## The expansion card system has a troubled story for battery life, but it's great for strain relief.

I appreciate that Framework have tried _really hard_ to let people customize their laptop with whatever ports they want. It's a great vision, but there are unfortunate power implications of always-on USB-C devices. But it's okay, because I don't need the full customizability of the expansion cards, _and you probably don't either?_ Bear with me here.

**The Framework is a USB-C laptop with a bespoke dongle system.**

![Sudden Clarity Clarence](../img/framework-review/clarence.jpg)

_All_ of the Framework expansion cards are USB-C peripherals that happen to fit into a nice custom slot. Whatever ports you install in your laptop are downstream of an internal USB-C port. (Well, technically a Thunderbolt port.) On one hand, this is a great design, because USB-C can do _everything_, from charge the laptop (USB-C PD) to drive 4k displays (DP alt mode).

On the other hand, it's not so great, because **USB-C peripherals tend to consume power.** Converting from USB to _anything else_ requires a high-speed microprocessor, and that microprocessor may not know to shut itself off when the 'anything else' is not in use, so the power management story [isn't consistently great](https://anarc.at/hardware/laptop/framework-12th-gen/#idle-power-usage-tests). Some of the expansion cards are battery vampires, using a few watts all the time ([even with the laptop on standby!](https://anarc.at/hardware/laptop/framework-12th-gen/#standby-expansion-cards-test-results)). Framework are working on this. They've had [three generations of HDMI card](https://knowledgebase.frame.work/en_us/hdmi-expansion-card-generations-Sk7AQKUv2) in their quest to make it consume less power. But it's still an issue, which means I generally don't want the power-consuming cards connected to my laptop _all the time_.

And Framework semi-quietly agree. You won't see it on the product pages, but in the [guide](https://knowledgebase.frame.work/en_us/optimizing-ubuntu-battery-life-Sye_48Lg3) for optimizing Ubuntu battery life:

> "One of the absolutely simplest things you can do right now to conserve battery power for your Framework is to simply remove unused expansion cards."

> "The best recommendation is to keep only expansion cards you need while on battery."

So we're supposed to keep the slots empty? Ugh, no. If you do, you expose sharp edges and corners. They catch on your hand when you pick up the laptop. Also, it looks like it's missing teeth. This does not spark joy.

[![Framework laptop with missing expansion cards](../img/framework-review/missing-cards-small.jpg)](../img/framework-review/missing-cards.jpg)

Given these tradeoffs, the whole internal dongle concept starts to make no sense. _How is this okay?_

For me, it's still okay. Here is why.

The two most versatile expansion cards are the most harmless. The USB-C card doesn't consume power at all. It's just a passthrough. The USB-A card is [_nearly_ as harmless](https://anarc.at/hardware/laptop/framework-12th-gen/#idle-power-usage-tests-306-beta-bios). You can keep a mix of these inserted without really affecting battery life. They're all the cards I need, and they're probably all anyone needs?

When working at my desk, a single USB-C cable powers the laptop and drives my monitor (with its built-in USB hub). If I'm out somewhere else, I can connect a mouse or charge my headphones with the USB-A ports.

Finally, if I want the _fullest_ complement of ports, I'll carry an omni-dongle.

[![USB-C port replicator](../img/framework-review/omni-dongle-small.jpg)](../img/framework-review/omni-dongle.jpg)

This one provides 8 expansion cards' worth of ports in a compact housing. Yes, it consumes a few watts during use. But for the times I need it, I'm almost certainly near a power outlet. For all other occasions, I don't leave it plugged in. 🙂

Am I building up to an argument that the Framework shouldn't have the expansion card system at all? Just regular USB-C and USB-A ports? No! I'm not, because the expansion card system carries another benefit that's unrelated to customizability.

**It virtually eliminates physical wear on the ports that are soldered to the main board.** The Framework's "exterior" ports (on the cards) take all the mechanical stress of repeated plugging and yanking sideways. On any other laptop, if you break a USB port, the repair is an entire new main board, expensive and difficult to replace. On a Framework, if you break an "exterior" USB port, you can buy a new card for a song, and replace it in about 5 seconds. That's fantastic.

If I were Mr. Framework, what would I do about all of this? Maybe some combination of the following.

- Position the non-vampire USB-C and USB-A cards as 'best for most people'. Maybe include 2 of each by default, with the other cards as separate add-ons.
- **Consider adding little physical power switches** to all the vampire cards (MicroSD, SSD, etc). The cards can remain in the laptop but switched off when not in use, drawing zero power.
  - (We already have these switches for the microphone and webcam!)
- In the battery life guide, recommend that people use USB-C cards instead of leaving slots empty.
- Don't sweat it too much, because this little company is doing _really [hard](https://community.frame.work/t/responded-amd-batch-1-guild/28693/123) [stuff](https://frame.work/products/laptop16-diy-amd-7040)_ right now.


---

The exception to all of this is the Framework's 3.5mm audio jack. It's the only non-USB-C port built into the laptop, on (edit:) [its own daughter board](https://frame.work/products/audio-board-kit). ~~soldered to the main board~~. ~~Yes, this means the consequences of breaking it are higher, but~~ The DAC behind it gets better power management, and you _always_ have use of it regardless of your expansion card configuration. (If you'd rather your headphone jack come on an expansion card, [you can still buy one](https://frame.work/products/audio-expansion-card).)

## 201 PPI display is fine with GNOME font scaling alone.

Some people seem convinced that the Framework's high-[PPI](https://en.m.wikipedia.org/wiki/Pixel_density) display is ill-suited for Linux. [^linuxincompatible] I strongly disagree. After avoiding high-PPI displays for years, I took the plunge and the water is fine. If you're okay using GNOME with Wayland (now the default on the most popular Linux distributions), it's easy to get a usable system with no blurriness. Click for full size:

[^linuxincompatible]: See [this thread](https://community.frame.work/t/tracking-state-of-hidpi-on-linux/8301/84) with 85 posts, some even calling the display "Linux-incompatible".

[![Framework screen](../img/framework-review/screen-pic-small.jpg)](../img/framework-review/screen-pic.jpg)

How do you get this?

1. Open Settings app, Displays section, set the Scale to 100%. Everything will be tiny.
2. Open Tweaks app, Fonts section, set the Scaling Factor to 1.37. Everything will be a more comfortable size. If you don't like that, try 1.25 or 1.50. [^distort]

[^distort]: If you need text larger than 1.50 font scaling, maybe try 200% display scaling instead, as font scaling above 1.50 starts to distort interface layouts.

This adjusts the size of both text and most other UI elements. It works for all the applications I use. You don't need to mess with fractional display scaling or `xrandr`. You don't end up with blurry Electron apps. I use video conference apps (like Zoom and Jitsi) in Firefox, and screen sharing works fine. When I use my low-PPI desktop monitor, I dial the font scaling back to 1.00.

I'm not claiming this will work for _all_ Linux users with all their edge cases, but I believe that the increasingly-well-trodden path of GNOME and Wayland is increasingly hassle-free. 

(I wrote a bit more about this [here](https://community.frame.work/t/guide-debian-12-bookworm-works-great-on-13th-gen-intel-framework-13/35432#display-observations-3).)

## 3:2 habits are different from 16:9 habits.

My last two Thinkpads have a 16:9 aspect ratio display, nearly twice as wide as tall. A 16:9 display isn't great for viewing a single document, because [longer lines become harder to read](https://en.m.wikipedia.org/wiki/Line_length). At a reasonable 6" column width, half of your 16:9 laptop display is "wasted" horizontal space that would have been more useful as vertical space for more lines of text. (Web sites _love_ put put ads here.)

![16:9 display with a single column of 6" wide text](../img/framework-review/display-comparison-16-9-single-col.jpg)

On the other hand, a 16:9 display _is_ great for using two windows side by side. Each half-pane is nearly square (8:9). If your 16:9 display is 14" diagonal, you get two 6.1" column widths to work with. It's somewhat close to notebook paper. A dual-pane layout can comfortably fit two editors, terminals, or (some) desktop-layout web pages, side by each.

![16:9 display with two columns of 6.1" wide text](../img/framework-review/display-comparison-16-9-dual-col.jpg)

I did this every day, generally with a 50/50 split, sometimes 60/40 (for taking notes next to something wider). [PaperWM](https://github.com/paperwm/PaperWM) makes this workflow easy.

Enter the Framework with a 13.5" 3:2 display (green rectangle below). About the same total area, but noticeably squarer.

![T480 versus Framework display size comparison](../img/framework-review/display-wars-comparison.png)

On one hand, the taller 3:2 screen fits about 10% more lines of text, which means 10% less need to scroll! It's nicer to work with a [slippy map](https://en.m.wikipedia.org/wiki/Tiled_web_map) or a diagram, because you get more north/south for your east/west. 3:2 being closer to square, it's _overall_ more versatile than 16:9.

On the other hand, the 3:2 display is less good for fitting two documents side by side, because a 5.6" column width can feel cramped. It's still good for working with fixed-width code or text: I can get 80-90 columns at a comfortable font size. But it's not so good for web pages intended for a desktop-sized viewport. Depending on the site, you get either a mobile layout (fine), or a horizontal scroll bar (yuck). A 60/40 split accommodates more sites, but still not all. [^6633split]

[^6633split]: I also tried out a 66/33 split, but the 33 is too cramped for anything other than note-taking.

Here is a to-scale-ish comparison of the 16:9 T480 display with the 3:2 Framework display. Click for full size.

[![T480 and Framework display size comparison diagram](../img/framework-review/display-comparison-edited-small.jpg)](../img/framework-review/display-comparison-edited.jpg)

So, while a 16:9 display feels limited on vertical space, after moving my 16:9 habits to a 3:2 display, I sometimes feel limited on horizontal space. Various tricks ameliorate this: Firefox reader mode, hiding unneeded sidebars in desktop apps. A few weeks in, I'm mostly settled into new habits. The biggest change is that some sites just really want the full display width.

[![screenshot of 2-column layout with cramped website](../img/framework-review/gitlab-screenshot-1-small.jpg)](../img/framework-review/gitlab-screenshot-1.png)

Web page is too crowded.

[![screenshot of full-screen website](../img/framework-review/gitlab-screenshot-2-small.jpg)](../img/framework-review/gitlab-screenshot-2.png)

That's better.

## The new default (3.5 kg) display hinge is fine.

The earliest Framework had display movement issues. The panel would wobble back and forth when you bumped the laptop, and it would droop when you carried the laptop around. I saw this first-hand with a friend's earliest-gen Framework (which he returned for several reasons). So, Framework started selling a stiffer (4.0 kg) hinge as a separate part, and I ordered one with my laptop.

But meanwhile, Framework also mitigated the wobble with a stiffer top cover, and mitigated the droop with a slightly stiffer default (3.5 kg) hinge. With my 13th gen, I can create wobbly and droopy scenarios, but they don't happen in typical use. The display moves a bit more easily than my Thinkpads, but it's also taller -- a longer lever on the hinge.

I haven't felt the need to install the 4 kg hinge.

## I don't miss my Trackpoint. The touchpad works fine.

I suspect a lot of [Trackpoint](https://en.m.wikipedia.org/wiki/Pointing_stick) users don't want to switch to a touchpad-only laptop, _because their Thinkpad has a crappy touchpad_. I was one of these people. The touchpads on my T480 were cramped. (This was okay, because it had a Trackpoint.) So, like [many others](https://community.frame.work/t/responded-any-chance-of-trackpoint/1026) contemplating a Framework, I was apprehensive about giving up the Trackpoint. The Framework's touchpad _had_ to be a _lot_ better than my Thinkpad's, and reader, it is. [^rsi]

[^rsi]: I think my switch away from Trackpoint was also an [RSI](https://en.m.wikipedia.org/wiki/Repetitive_strain_injury) improvement. The tip of my index finger no longer gets sore after hours, because it's not shoving a rubber nub anymore, only gliding on glass.


Tapping, right-clicking, and dragging all feel good. 3-finger gestures work too. The experience feels _approximately_ as intuitive as my 2008 unibody Macbook. (I think the Macbook was slightly better, but I suspect that was due to Apple's drivers.) I'm grateful that the touchpad isn't _so_ big that I'd have to rely on software to ignore accidental brushes while typing (even though GNOME Tweaks has an option for this). It's the right size.

## But I sorely miss page up/down keys.

This is my biggest usability issue with the Framework. On other keyboards, I use these keys all the time for navigating code and documents, and also for navigating workspaces. I've neuroplasticized around chords like (`shift` +) [`super`](https://en.m.wikipedia.org/wiki/Super_key_(keyboard_button)) + `page up/down` to manage workspaces, and (`shift` +) `ctrl` + `page up/down` to manage tabs. Thinkpads encourage this because it's easy to locate page up/down without looking.

On the Framework keyboard, to send a page up/down event, you must chord `fn` with the arrow keys. So my prior two-key chords become three-key chords, which isn't so bad. I'll get used to that. But prior 3-key chords become 4-key chords (like `fn` + `shift` + `ctrl` + `up/down arrow`), which are  awkward to type. Further, _any_ use of page up/down is necessarily a two-handed operation. If I'm reading while eating something messy with one hand, I need to scroll another way.

Most laptop manufacturers cut this corner, and it hurts. But it seems there's an opportunity to introduce page up/down keys on the Framework without an input cover redesign. The left and right arrow keys could become shorty short, like the up and down arrow keys already are. This would make room for page up/down above the left and right arrows. The resulting layout would be like a Thinkpad, just a bit smushed. ([Some Dell XPS laptops](https://img.devrant.com/devrant/rant/c_1817945_CZnhS.jpg) already do this.) Importantly, it wouldn't require any new holes in the input cover, which may preserve parts compatibility and limit SKU proliferation.

I would easily pay $100 for such a keyboard.

## Charge termination at 4.45 volts per cell!!?

(This section goes way outside my expertise. If you have expertise with lithium-ion chemistry, please correct any mistakes and I will update this post.)

Edit: sure enough, [@Kyle_Reis pointed out](https://community.frame.work/t/framework-13-things-that-surprised-me/36235/2) the battery announcement that mentions 4.45 volts per cell. Not sure how I missed that the battery is definitely supposed to do that. Putting what I originally wrote in a blockquote.

> The standard guidance for lithium-ion chemistry is to [terminate charge at 4.2 volts per cell](https://en.m.wikipedia.org/wiki/Lithium-ion_battery#Performance). A charge termination voltage _higher_ than 4.2 V stores a little more energy, but sacrifices battery health [^plating] (and possibly safety). A _lower_ charge termination voltage under-fills the bucket, keeping the battery healthier for longer. This is why Tesla encourages their owners to [charge only to ~~90%~~ 80%](https://www.macheforum.com/site/threads/tesla-now-recommends-charging-to-80-daily-instead-of-90.29858) (🤭) ([archive](https://web.archive.org/web/20230902052344/https://www.macheforum.com/site/threads/tesla-now-recommends-charging-to-80-daily-instead-of-90.29858/)), which corresponds to a lower-than-design-maximum charge termination voltage. [^teslalifepo4] So, my prior understanding is that you keep lithium-ion cells healthy for a long time by avoiding extreme states of charge. The gentlest cycles are shallow ones, not going above 4.2 volts per cell, not going much below 3 volts.
>
> So I charged up my Framework on the first day. At 99% state-of-charge, I read out `/sys/class/power_supply/BAT1/voltage_now`, and I see 17.8 volts. [^kerneldoc] Assuming that's across 4 cells in series, we divide by 4 to get 4.45 volts per cell! 😵 At first this sounded erroneously high, but Framework's [product photo](https://frame.work/products/battery?v=FRANGWAT01) for the 61 watt-hour battery also shows a "Limited Charging Voltage" of 17.8.
>
> For lithium ion chemistry, 4.45 volts per cell is _really high_. How is this okay?
>
> The kernel reports a battery manufacturer of "NVT" and a model name of "FRANGWA". This doesn't turn up _anything_ in a web search. But Framework's [product photo](https://frame.work/products/battery?v=FRANGWAT01) shows a manufacturer of Dongguan Amperex Technology Limited. A web search for "Dongguan Amperex 4.45" returns [a 2012 patent](https://patents.google.com/patent/CN102610806A/en) and [a 2015 patent](https://patents.google.com/patent/CN105428701A/en) from this company. Both mention cell voltages of 4.4 and higher. It sounds like they developed a new anode material and a new electrolyte. I also found [these "high voltage cells"](https://www.grepow.com/lihv-battery/lihv-battery-4-45-v.html) ([archive](https://web.archive.org/web/20230902055343/https://www.grepow.com/lihv-battery/lihv-battery-4-45-v.html)) that list a charge termination voltage of 4.45. Maybe rebadged Dongguan Amperex cells? I have no idea. Finally, I also see references to LiHV and [even ULiHV](https://www.rctech.net/forum/radio-electronics/1097620-ulihv-batteries.html) on a few drone hobbyist sites.
>
> It _seems_ that the battery is expected to handle 4.45 volts per cell. The only alternative is that Framework push the battery _very_ hard to squeeze a few more watt-hours. I'm hoping that's not the case, because it would not bode well for battery longevity.
>
> Maybe someone has a data sheet? 🙂 In the meantime, for lack of better information, I set a maximum charge level to 90% in the BIOS. This appears to terminate charging at 17.21 volts, or 4.3 volts per cell. I'm happy to give up some capacity to gain some longevity. But again, I'm out of my depth. Someone who knows more, please check my work.

[^plating]: I'm not going to pretend to understand how anode plating works, but [Wikipedia claims](https://en.m.wikipedia.org/wiki/Lithium-ion_battery#Detailed_degradation_description) "Charging at greater than 4.2 V can initiate Li+ plating on the anode, producing irreversible capacity loss." [Battery University](https://batteryuniversity.com/article/bu-409-charging-lithium-ion) talks about this and also some safety aspects.

[^teslalifepo4]: For all but the "standard-range" models with [LiFePo4 chemistry](https://en.m.wikipedia.org/wiki/Lithium_iron_phosphate_battery), which happily tolerates thousands of full-depth cycles.


[^kerneldoc]: [Here](https://www.kernel.org/doc/Documentation/ABI/testing/sysfs-class-power) is the kernel documentation for `/sys/class/power_supply`. It displays `voltage_now` in microvolts, so I'm dividing the raw numbers by 1,000,000 to get whole volts.

## Things that _didn't_ surprise me but are worth mentioning.

- Linux support is [overall great](https://community.frame.work/t/guide-debian-12-bookworm-works-great-on-13th-gen-intel-framework-13/35432) with a modern kernel. Nearly everything just works.
- By modern standards, the battery life is merely _okay_, even with the 13th gen's larger 61 watt-hour battery, even if you avoid all the vampire expansion cards. You're mostly limited by the high-strung Raptor Lake CPU. (Your ARM Mac friends will humble-brag on this point.)
    - I wrote more about observed battery life [here](https://community.frame.work/t/guide-debian-12-bookworm-works-great-on-13th-gen-intel-framework-13/35432#power-consumption-observations-4). It's about 6 hours of my typical work.
    - For those who need more endurance, the obvious solution seems to be carrying a USB-C PD power bank. They easily double your runtime.
- The display can get very bright, has great colors, and a great anti-glare finish.
  - 60 hertz is too slow for a few people, but I've been careful to avoid using anything faster that could ruin my taste for 60 hz.
- Missing keys aside, the keyboard is quite nice to use. Feels good, sounds good, no complaints about key travel.
    - As with my Thinkpad, I can cause [Typeracer](https://play.typeracer.com) to accuse me of cheating.
    - To preserve some Thinkpad muscle memory, I swapped the `fn` and `ctrl` keys in the BIOS.
- The webcam is noticeably better than my T480's. It's sharper with better detail in the shadows.
- Assembly of the DIY edition was easy.
    - The one fiddly part was getting cables to lay flat enough around the right-hand display hinge for the bezel to sit flat. After a few tries, it was good.

## Closing Thoughts

Do I _like_ my Framework laptop? Hell yes I do. Moreover, I like what the company is doing. This is a [tiny startup](https://www.crunchbase.com/organization/framework-52d4/company_financials) compared to Lenovo, Dell, or Apple. And they're doing _hard stuff_. Their repairability crusade might be dragging [a 10,000x larger company](https://www.wsj.com/articles/apple-opens-self-repair-store-with-300-iphone-screens-19-cent-screws-11651031828) along for the ride. It's certainly proving what's possible to consumers (and to regulators!).

I really appreciate that Framework are centering Linux support. Linux users are nitpicky in a different way than Mac users. Linux users tend not to care if the computer's panel gaps are a millimeter off. But they _do_ care about upstream kernel support for every last chip inside. They care about open-source UEFI firmware (which Framework hasn't achieved yet, but it seems possible). It's a prickly community to deal with, and it's a tiny fraction of the overall PC market. But Framework are serving them.

That's essentially why I bought a Framework while my T480 had plenty of life left, and why I'm writing this post. I want this team to win.