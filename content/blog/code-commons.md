---
title: "Vision for Code Commons"
date: 2022-09-05T15:18:36-07:00
tags: []
draft: false
---

People who make computers do new things often work physically alone. Solitude can enable productivity, but it can also stagnate creative work. Fellowship and collaboration enriches life and craft. So, people who work with code can establish public gathering places, like libraries. They can meet to share experience, mentor and learn, help each others' projects, and discover opportunities. They can also just work quietly in the presence of others doing the same.

Building this community could especially help grow free, libre, open-source software (FLOSS), and software built for public service missions. Contributors to these projects are often physically isolated from other developers. I believe that isolation contributes to attrition. We can build community and culture around creating great software for humanity, and this could yield enormous value for humanity. That is the essential idea of Code Commons.

## How could it work?

Find a local public space with table seating for a small group. A library seems well-suited, as it's already a public intellectual space. [^notlib] Talk to the people in charge. Ask if it's okay to have a small, informal gathering.

Start with simple days and times to meet, such as 'Wednesday afternoons'. [^dedicatedspace] Advertise this around your local meetups, professional organizations, reddit, and schools that teach programming. Make a "Code Commons" tabletop sign to help others find you.

Show up with work to do, but with flexibility in your day to meet others and have conversation. See who shows up and what happens. Evaluate and grow the event according to the local group's needs. Maybe people want quiet focused co-working in addition to conversation. You can separate those activities in space (with different tables) or in time (with separate quiet and conversational hours). Some people may want to spend every day there. The possibility space includes all of this and more.

People could establish Code Commons in every city in the world. 99% of Code Commons would be the sum of these local chapters. [^travel] The remaining 1% would be a very lightweight central organization to connect them. [^org]

## Rationale and Impact

Millions of knowledge workers are now un-tethered from a physical office, many of them permanently. This has complex and not entirely good effects. [Here](https://www.nytimes.com/2022/08/16/opinion/ezra-klein-podcast-anne-helen-petersen-charlie-warzel.html) is a random (but relevant) exploration. I appreciate the ability to work for a geographically distant employer, but I miss having a place to go every day, and a community of practice to be anchored in. Code Commons can provide that anchor for people who work with code, maybe in a better way than traditional workplaces did.

Code Commons could also increase retention of people building FLOSS, especially at places like universities. Corporations infused with investor cash can offer highly-compensated developer jobs, but they usually cannot offer a borderless collaborative environment for creative problem-solving. Code Commons can deliver that, and it will enable more people to start (and continue) working on FLOSS.

Additional things that I want Code Commons to enable:

- Raise the waterline of computational literacy for everyone, and fill up the pipeline of new software developers, because the world needs more of them.
- Make FLOSS more user-friendly. A lot of people in my life (including non-technical family) now use a Linux-based distribution as their desktop operating system. How can we make that better for more people?
- Expand the sphere of activities that happen outside of platforms and devices powered by surveillance capitalism. Enable more [protocols, not platforms](https://knightcolumbia.org/content/protocols-not-platforms-a-technological-approach-to-free-speech).
- Produce more data science in the public interest.

## Who is and isn't welcome?

Code Commons welcomes you, whether you're learning programming for the first time or you have worked with code for decades. Whether or not people would stereotype you as a "software engineer". Whether or not you've had any computer science education.

Maybe your work is mostly research with incidental programming. Maybe you're in a less-technical role like project management. Maybe you make mostly-physical, incidentally-digital things. [^makerspace] Maybe you use a computer in creative ways that are not programming, like writing, design, and making art. Maybe you're a civic hacker, using public data in ways that inform policy and governance. Maybe you build proprietary software for a living, but you are open-minded about FLOSS. Code Commons welcomes all of these people, and more.

You are probably _not_ welcome if:

- You are there mostly to sell a product or service to attendees.
- You are a recruiter seeking attendees' personal information to sell access to their skills. [^recruiter]

Also, you are not welcome if you would treat other attendees in a manner that a professional workplace would not tolerate. [^conduct]

If still unclear, I think people should show up and find out! Every local chapter will develop a culture of its own. [^disagreement]

## Next Steps

I am starting the first chapter where I live in Tucson, Arizona, USA. Stay tuned for the coordinates!

If you live somewhere else, try it out there too. Tell me how it went and I will signal-boost it here.

I am confident about some aspects of this idea, but not about some others. For example, what is the right mix of social time versus quiet co-working time? What's the best sort of location to provide both convenience to students (who may have difficulty attending off-campus) and convenience to others? [^drive] Let's experiment and evaluate what works.

## Prior Art

A lot of inspiration from Code Commons comes from [Research Bazaar Arizona](https://researchbazaar.arizona.edu), an organization that delivers something like Code Commons, but scoped to researchers. It also comes from meetups such as the [Tucson Python Meetup](https://www.meetup.com/Tucson-Python-Meetup/).

## Comments

If you have questions, ideas or experience to share, email me, chris at c-mart dot in. Tell me if it's okay to post your message here as a comment.

## Footnotes

[^travel]: Imagine visiting another city's Code Commons while traveling. It could be fun to connect with an entirely new group.

[^notlib]: Alternatively, hosting Code Commons at a place that serves food and coffee/tea could make it easier for people to engage. Perhaps at cafe or fast-casual restaurant that would tolerate a group's presence for a few hours. Also, this is a USA-based perspective, but I think Code Commons should avoid places where people typically drink alcohol. Mostly because Code Commons focuses more on work than leisure, but also because the presence of alcohol may exclude a few categories of people who Code Commons should definitely serve.

[^recruiter]: For a software developer, unwanted attention from recruiters can be a common nuisance and a deterrent from attending events. There are plenty of places to interact with for-profit recruiters, and I believe Code Commons should not be another one of them.

[^disagreement]: A common pattern in intentional communities is people having different, sometimes-conflicting ideas on how a group should and shouldn't operate. In a large enough city, multiple chapters of Code Commons could evolving in parallel to serve different niches. Also, if Code Commons grows to many chapters, it is likely that in some place, the first person to organize a chapter will exhibit toxic or contemptible behavior. In that event, I want to empower others in that place to start a new chapter with different organizers, without unnecessary friction. It should be as simple as forking a git repository.

[^conduct]: Perhaps the central organization offers a lightweight code of conduct, and each chapter can use or customize it if they want.

[^dedicatedspace]: I imagine the most advanced stage is a dedicated physical space for Code Commons that is open all day, every day. There would be more-populated and less-populated times. Some people may show up for an hour or two occasionally, while others may work their entire full-time job there.

[^drive]: In my experience, the older people get, the more they prefer to drive (instead of walk or bike) to places, and the less appetite they have for negotiating traffic and paying for parking at a university. I don't know how to square this with the need for everyone to drive less for climate's sake.

[^org]: This would consist of a website (I registered codecommons.net), a directory of local chapters, and a non-proprietary [^discourse] global communication tool. Maybe this organization also defends Code Commons (as a trademark) against parties who would use the name for something that is incompatible with the mission. I guess I would lead this organization to start, but it would be healthy in the long-term to share the leadership role with others.

[^discourse]: Code Commons should have a communication platform that is not Slack, Discord, reddit, or any other proprietary software that is under the thumb of a large corporation. A self-hosted [Discourse](https://en.wikipedia.org/wiki/Discourse_(software)) could work great for this, or even a mailing list with the archives posted online. I don't see a strong need for real-time chat, but I believe [Element](https://element.io/) on the [Matrix](https://matrix.org/) protocol would serve that role well.

[^makerspace]: Code Commons could be described as a software-oriented maker space, and I think it is very compatible with established physical maker spaces.