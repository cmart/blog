---
title: "Thought Experiments on Transit and Housing"
date: 2024-01-06T20:11:00-07:00
tags: []
draft: false
---

![A concrete apartment building modified to function as a city bus. The building has multiple floors, typical apartment windows, and balconies, but is set on large bus wheels. It includes a front door designed like a bus door, and the sides display bus route information. The setting is an urban street, illustrating the contrast between this unique bus and the surrounding cityscape.](../img/transit-housing/apt-building-as-bus.png)

Every major city in the USA provides transportation as a public service. [^cities-without-transit] Not only streets, but also _vehicles_, with professionals who drive them, and _anyone_ can ride these vehicles, for a price between free and cheap! And it's not intended only a service for the poor: transit agencies encourage _everyone_ to ride.

These services are expensive to provide, [^cost] and _all_ of them operate at a loss. [^fbr] To continue existing, transit agencies require ongoing funding from taxpayers, _the vast majority of whom don't use transit_. [^donotuse] Still, public transit enjoys public support. Small-government republicans clamor to de-fund many things, but transit barely makes their list. [^gop]

We accept the high cost of transit because it raises the floor of personal mobility, enabling access to employment and critical services. You can ride the bus no matter your means or circumstances. You can still ride it if macroeconomic forces make cars unaffordable. The bus may be inconvenient or smell weird, but it gets you around. (And if you're unable to walk or roll to a bus stop, there's even paratransit that stops directly at your home and your destination.)

In this context, two thought experiments. (I am very not an expert. Please tell me how I'm wrong.)

## What if housing worked like transit?

Imagine that a city-owned agency builds hundreds or thousands of modest apartments on city-owned land. These exist in many neighborhoods, especially near where people work, shop, and recreate. Consultants are hired to site these buildings where they serve the greatest demand.

Anyone can rent an apartment for a rate subsidized to perhaps a third of their income. A six-figure-earning professional pays $2750/month, while a single mom who waits tables pays $750/month. Nobody is denied housing because they cannot afford it.

The full, unsubsidized rent rate is more than the financed cost to build the apartment, yet the housing agency still loses money overall, because many of the units rent out for a much lower rate. Local tax revenue and federal grants make up the difference, and somehow, the voting public accept this arrangement.

Imagine there are almost always apartments available. If they are filling up, the city is generally building more. It's rare for there to be a long-term, city-wide shortage that would force people to move to another region. Not many people are homeless, because there is a well-funded agency with a mandate is to _build as many affordable homes as the public needs_.

**Intuitively, it would reduce a lot of human suffering by reducing homelessness.** It would enable more economic activity, because it's a lot easier to have a job when you have a home. It could correct market failures that have caused housing scarcity and unaffordability.

### That sounds nice, but what are the drawbacks?

First, America has forgotten how to build things. [^forgot] We would need to become better at that again, which challenges the structure of contemporary politics.

Second, it would cost a lot. Maybe an order of magnitude more than transit? A _very wild_ guess is $2500 per capita per year for the most populous and housing-stressed cities. [^guess] $2500 per capita per year is about 25% of San Francisco's budget, or _all_ of Houston's budget. [^city-state-spending-per-capita] But hey, it's only about a fifth of total healthcare spending! [^healthcare]

I see a lot of externalized public cost savings, because it would reduce the number of under-housed people who burden other public services, such as hospitals and homeless shelters. It would also indirectly increase the number of employed people, which expands the economy and tax base. But I digress, you asked about drawbacks.

Maybe the apartment buildings are kind of sketchy, because they house a lot of people who aren't in the habit of caring for their home. But there would still be rules, and repeatedly breaking them gets you kicked out, so renters who create a hostile or dangerous situation for their neighbors do not get to stay. Maybe the most difficult-to-house people are not suitable for these public apartments, and we'd have to figure something else out for that.

Maybe it cools the real estate market by massively increasing housing supply?

Maybe it's a raw deal for existing landlords, who now have to compete with more public housing? But that's already true of taxi and ride sharing services, and those seem to be doing fine.

## Alright. What if transit worked like housing?

Imagine that _public_ transit mostly doesn't exist. Some cities operate a few _old_ buses and trains, but only a handful of people who have been riding them for 40+ years are allowed to use them today. There is a several-years-long wait list to become a new rider. In effect, it's not available to most people.

Instead, some fraction of the money that would fund a transit agency instead funds a voucher program for _private_ transportation. Low-income people can undergo a bureaucratic, means-tested process to apply for these vouchers. Taxi, shuttle, and ride share operators can choose to accept vouchers as payment.

Most operators don't accept these vouchers, perhaps due to burdensome requirements. The driver must hold a CDL, their vehicle must undergo rigorous inspections, and it can fail for reasons like "the bumper is dented". Also, perhaps for reasons correlated with socioeconomic status, the riders who pay with vouchers are disproportionately likely to leave a mess in the car, smoke meth during the ride, assault the driver, and so on -- this isn't the business you want.

The operators that _do_ go to the trouble of accepting vouchers tend to be petty sociopaths who abuse the people who have reduced freedom of choice to use another service. Maybe they're an hour late picking riders up. Maybe they turn around and scream at riders in the back seat for no reason. These actions are calculated to avoid getting themselves into actual trouble, but they make the rider experience intermittently miserable.

So, perhaps the "rides to places" market bifurcates.

| **Vouchers**    | **Price**             | **Availability** | **Experience**    |
|-----------------|-----------------------|------------------|-------------------|
| Does not accept | What market will bear | Plentiful        | Pleasant          |
| Accepts         | Free for user         | Scarce           | Crappy            |

In some places, the market will bear particularly high transportation prices (e.g. because parking is particularly expensive). The result is that low-income people mostly cannot access affordable transportation, and so have severe difficulty accessing services far from where they live.

Those places will tend to be wealthier, but also have aggressive hitchhiking and high rates of carjacking. Some people, at great personal risk, will jump on the back of cars that pull away from stoplights, holding onto the roof rails to catch a ride from a non-consenting driver.

Separately from the voucher system, new taxi medallions and ride share permits also require licensees to offer a few percent of their rides at below-market rates for their first decade or two, and it's reimbursed with a tax subsidy, or something. We call these "Affordable Rides" to disguise how inadequate this system is. It's nice for the folks who receive the discounted rides, but demand for these rides much exceeds supply, so they are allocated with something like a means-tested lottery system or a wait list. In effect, no one who wants a discounted ride can _reliably_ get one, even if they qualify.

### That sounds mostly bad, but are there benefits?

It would be cheaper to fund than public transit, albeit with a lot of externalized costs, because lower-income people have much-reduced mobility.

It might be climate-positive, because we're no longer operating mostly-empty diesel buses that get 3 miles per gallon?

## Lingering Questions

In the latter 20th century, funding and support for public housing was [drastically curtailed](https://en.m.wikipedia.org/wiki/Subsidized_housing_in_the_United_States), while in 2024, public transit remains funded and available in nearly every city: just get on the bus. Why did public housing and public transit have such different fates?

Are there things we can learn about transit to inform strategies for changing housing policy? (And maybe vice versa?)

It's widely socially accepted to kick someone off a bus for bad behavior, but it's much more fraught to evict someone from their home. What other aspects of this analogy break down?

What did I get wrong here?

[^cities-without-transit]: Of the 300+ US cities with populations of at least 100,000, [only five of them](https://247wallst.com/special-report/2018/11/27/americas-largest-cities-with-no-public-transportation/) ([archive](https://web.archive.org/web/20240000000000*/https://247wallst.com/special-report/2018/11/27/americas-largest-cities-with-no-public-transportation/)) have no fixed-route public transit service. (And only one of those, Arlington TX, is much bigger than 100,000.) The source looks a bit content farm-y, but their methodology appears good, looking at census data where 0% of respondents ride transit to work.

[^cost]: A regular transit bus costs half a million dollars, and burns a gallon of diesel every 3 miles, all day long. Electric buses cost a whole million dollars and separately require expensive charging infrastructure. ([See page 9 here for example costs (PDF)](https://www.actransit.org/sites/default/files/2022-06/0105-22%20Report-ZETBTA%20v3_FNL.pdf) ([archive](https://web.archive.org/web/20240000000000*/https://www.actransit.org/sites/default/files/2022-06/0105-22%20Report-ZETBTA%20v3_FNL.pdf)).) A large city will have several hundred buses. You also need a typically-unionized staff of drivers, mechanics, dispatchers, and data science consultants to plan routes. You need a big, expensive maintenance facility. You need specialized software to manage schedules and fare payments. So, it's expensive. A random example: [the Alameda-Contra Costa Transit District](https://www.actransit.org/facts-and-figures) covers Oakland and several other California cities. It costs about a half-billion dollars per year, and serves an area with population of 1.5 million, for a per-capita cost of about $350 per year. That's $350 _for every resident_, even though only a fraction of them use transit. 

[^fbr]: [Farebox recovery](https://en.m.wikipedia.org/wiki/Farebox_recovery_ratio) measures the proportion of money spent on a transit service that is recovered by fares paid by riders. A couple of weird outlier agencies approach break-even, but most of them in the USA recover less than half of their own cost.

[^donotuse]: [Census report](https://www.census.gov/newsroom/press-releases/2021/public-transportation-commuters.html) ([archive](https://web.archive.org/web/20240000000000*/https://www.census.gov/newsroom/press-releases/2021/public-transportation-commuters.html)) showing that "About 5% of all U.S. workers in 2019 commuted by public transportation", increasing to 11.5% in the largest cities.

[^gop]: [2016 Republican Platform (PDF)](https://prod-cdn-static.gop.com/media/documents/DRAFT_12_FINAL%5B1%5D-ben_1468872234.pdf) ([archive](https://web.archive.org/web/20240000000000*/https://prod-cdn-static.gop.com/media/documents/DRAFT_12_FINAL[1]-ben_1468872234.pdf)) mentions "transit" 3 times in a 66-page document, compared to about 35 mentions of "healthcare".

[^guess]: Assuming an apartment costs 10x that much to finance, and we end up building apartments for 10% of the population.

[^city-state-spending-per-capita]: [Ballotpedia: Analysis of Spending in America's Largest Cities](https://ballotpedia.org/Analysis_of_spending_in_America%27s_largest_cities) ([archive](https://web.archive.org/web/20240000000000*/https://ballotpedia.org/Analysis_of_spending_in_America's_largest_cities))

[^forgot]: Pick your source of [Why Doesn’t America Build Things?](https://www.vice.com/en/article/93a39e/why-doesnt-america-build-things) ([archive](https://web.archive.org/web/20240000000000*/https://www.vice.com/en/article/93a39e/why-doesnt-america-build-things)), [Why does it cost so much to build things in America?](https://www.vox.com/22534714/rail-roads-infrastructure-costs-america) ([archive](https://web.archive.org/web/20240000000000*/https://www.vox.com/22534714/rail-roads-infrastructure-costs-america)), or any of a hundred things Ezra Klein has written [or said](https://news.berkeley.edu/2023/10/20/berkeley-talks-transcript-ezra-klein) ([archive](https://web.archive.org/web/20240000000000*/https://news.berkeley.edu/2023/10/20/berkeley-talks-transcript-ezra-klein)).

[^healthcare]: [Center for Medicare and Medicaid Services](https://www.cms.gov/data-research/statistics-trends-and-reports/national-health-expenditure-data/historical) ([archive](https://web.archive.org/web/20240000000000*/https://www.cms.gov/data-research/statistics-trends-and-reports/national-health-expenditure-data/historical)) shows $13,493 total healthcare spending per person in the USA, in 2022.