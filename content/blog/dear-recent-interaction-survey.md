---
title: "Dear Recent Interaction Survey,"
date: 2024-04-08T16:00:00-07:00
tags: []
draft: false
---

![screenshot of survey invitation](../img/dear-recent-interaction-survey/survey-invite.jpg)

It seems that you want my help making a personnel decision?

Sadly, I have little to say about the agent who answered my phone, email, or chat inquiry. They appeared to be a competent human. If I was unknowingly interacting with an LLM, you did a great job fine-tuning it. While I'd have preferred someone with forensic troubleshooting skill and deep knowledge of your systems, I understand that organizations move such people [far away from the general public](https://www.bitsaboutmoney.com/archive/seeing-like-a-bank/).

You didn't ask, but I often have feedback _about the service itself_ that your (hospital network, airline, car insurance company, etc.) delivers.

Which aspect of the customer experience caused me to initiate a customer service interaction? Could you make a [positive-sum](https://en.wikipedia.org/wiki/Win%E2%80%93win_game) change to your _socio-technical systems and processes_ to avert such situations in the future? If you can't eliminate the problem, can you make its solution obviously self-service?

Do your engineering and operations teams [test-drive](https://en.wikipedia.org/wiki/Eating_your_own_dog_food) the firm's own customer experience? Do they have both the [slack](https://thezvi.substack.com/p/slack) to notice opportunities like these, and the freedom to actually make changes?

I fear that, by framing your questions [in terms of the person](https://en.m.wikipedia.org/wiki/Henry_Thomas_Buckle#'Buckle's_Trichotomy') who I interacted with, you are transmuting any problem I have with your organization's service into _a problem with a named low-status employee_. This human, constrained by your systems, doesn't deserve the blame.