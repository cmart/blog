---
title: "Building a New Blog"
date: 2022-02-06T19:49:42-07:00
tags: ["meta", "work in progress"]
draft: false
---

## Background

(talk about blog.c-mart.in)

## Planning and Design

### Things I Want in a Blog

- Compose with Markdown. In 2015 this was rare, now everything supports it.
- Image etc hosting? image galleries?
- I don't want to sysadmin it or self-host it, but I want the option of easy migration to self-hosting if the hosting turns evil.
- Resistant to slashdot/reddit/HN effect, maybe a CDN achieves this
- Option of automatic syndication to Twitter, Facebook, etc
- Easy rich discussion, in comments or via some other mechanism
- Email notification of comments, to me and other commenters
- Don't make me think about all the minutiae of setting up a website (e.g. layout and styling)

### What other people do

- Scott Alexander uses Substack
- Rob Rhinehart uses Ghost?
- [Gwern uses Hakyll](https://www.gwern.net/About#technical-aspects)

### Comment Systems Exploration

[Self-hosted Discourse](https://github.com/discourse/discourse/blob/main/docs/INSTALL-cloud.md) could work but I'd have to host it.


There is [Commento](https://commento.io/pricing) but it has non-transparent pricing over 50,000 daily page views


#### Comments on GitLab Issues

Idea: an issue on GitLab for each post, and the comments on the post are comments on the issue. This would satisfy "don't want to self-host but could if the hosting turned evil". I have built and administered on-premises/self-hosted GitLab professionally, it's not that hard. gitlab.com allows you to migrate projects to your own self-hosted GitLab.

Prior art using GitHub issues for comments:
- https://utteranc.es/
- https://pknopf.com/post/2018-10-13-comments-for-static-sites-using-github-issues/

Does the Wayback Machine capture gitlab issues? Apparently not: https://web.archive.org/web/20220202033418/https://gitlab.com/exosphere/exosphere/-/issues/658

But it does archive GitHub issue comments:
https://web.archive.org/web/20220101075010/https://github.com/reactjs/reactjs.org/issues/3308

This may be why:
https://about.gitlab.com/blog/2017/11/09/gitlab-vue-one-year-later/
https://gitlab.com/gitlab-org/gitlab/-/issues/215365

> We now render all issue comments in Vue.

## Implementation

- Started with: <https://zwbetz.com/make-a-hugo-blog-from-scratch/#tweak-blog-list-layout-title>
- Got rid of bootstrap, replaced with a few hand-written CSS declarations from [Better Motherfucking Website](http://bettermotherfuckingwebsite.com)
- Used [System UI Fonts](https://www.smashingmagazine.com/2015/11/using-system-ui-fonts-practical-guide/)

---

## To do
- Build front page
  - Preview most recent posts on the front page
- Load icons from not JS, so it works with JS disabled
- Ensure it looks great in Firefox reader mode, including images
- Figure out how host images. B2? R2?
- Figure out comments as GitLab issues
- Plan migration of content from old blog
- Is user-selectable dark/light mode possible without JavaScript?
- Table of contents for longer posts?
- Show original post date and last updated date