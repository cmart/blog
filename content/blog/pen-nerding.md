---
title: "Pen Nerding"
date: 2023-01-23T22:33:00-07:00
tags: []
draft: false
---

You might think paper writing is obsolete, or there aren't interesting developments in writing technology anymore. I was surprised to discover:

- It's very relevant, even to a software engineer (more on this later).
- There has been a lot of innovation, mostly from Japan, in the 15 years since I last bought "school supplies".

Here are preliminary, personal conclusions after a few months of casual nerding.

![a picture of a pen, taken apart, with the ink refill all used up](../img/pen-used-up.jpg)

## Gel Ink

Pentel EnerGel, 0.7mm tip, is best gel. I found it a little better than Uni/Uni-ball, and a _lot_ better than Pilot G2. The biggest difference is that it's the least-smeary and fastest-drying of these. It's super low-drag (and so non-fatiguing) to use. It resists skipping well until the very end.

It's about the only ink refill that I've used up completely (photo above), instead of suffering some malfunction or losing it.

This finding roughly aligns with the advice of 'product recommendation site owned by giant newspaper'. But as far as I know, the next section covers new ground.

## Body/Refill Looseness

A modern pen has two main parts. The _body_ is what you hold in your hand. The _refill_ goes inside the body, holding the ink and dispensing through its tip.

You want the tip of the pen body to hold the tip of the refill _snugly_, without looseness. This lets your writing be more precise, and it reduces the need for hand movement.

I started measuring this with a vernier caliper: subtract the outside diameter of the refill tip from the inside diameter of the hole in the body. The difference is a measure of looseness. You want that number to be low.

I've found that Uni-ball and Pentel pens consistently have _under one thousandth of an inch_ of looseness, usually about half a thou. (Sorry, non-Americans.) I measured about 1.5 or 2 thou of looseness in my Pilot G2 pens, and between 6 and 10 thou in an assortment of cheap promotional pens. So, another reason to favor Uni-ball and Pentel gel pens over the Pilot G2.

I also learned that Pentel and Uni-ball pens all have the same refill tip diameter, about 0.0905" in an 0.091" hole. This means you can mix refills and pen bodies between those brands, and get a snug fit either way.

## Pen Bodies

EnerGel refills are great, but I don't like most EnerGel bodies, because they're trying to look like jewelry, running shoes, and/or a spaceship. The EnerGel Kuro is perhaps the one exception. Very simple, great pen. I found some at the 'red circle with a dot in the middle' store.

My _most_ favorite pen body is the Uni-ball One (taken apart in photo above). It's pure and simple utility. The metal clip is sturdy and spring-loaded. I like the black-body Uni-ball One, but white is fine too.

Unfortunately, the refills that come inside Uni-ball One pens feel scratchy and skippy to me (perhaps because they're all fine or extra-fine point). I took them out and installed EnerGel refills, which fit great in Uni-ball One pens. Best of both worlds.

## Jetstream Ink

(No relation to the research cloud service that I work on, called Jetstream2.)

A Uni-ball Jetstream pen is surprising to use for the first time. The ink is oil-based, like you find in 'cheap' (Bic-style ballpoint) pens, but something in Jetstream ink allows the pen to move with a lot less drag than you'd expect from that sort of pen. The ink looks a bit darker than ink from a cheap pen, too.

In my experience, Jetstream ink _approaches_ the effortlessness of writing with a good gel pen, but doesn't match or exceed it. So, it's not my _favorite_ for overall use, but it's not far behind.

That said, here are two reasons one might pick Jetstream ink over gel ink.

1. It is waterproof. Gel ink can be _kind of_ waterproof, but it runs if the paper gets wet. Jetstream ink doesn't.
2. Jetstream ink refills are skinny enough to fit a several of them inside a pen body of reasonable size. This allows for _multi-pens_: multiple ink colors in the same pen, with ink that feels nice to write with.

I have a 4-plus-1 Jetstream pen. It's slightly thick to hold, but it has black ink, blue, green, red, _and_ a mechanical pencil, _and_ an eraser in the top. Amazing.

## Where to find these pens?

In decreasing order of awesomeness:

- A Japanese stationery store like Maido / Kinokuniya. I found one while in a big city.
- jetpens dot com. Not just a store, they have a blog with some helpful guides.
- Your local big box office supply store or 'clothes but also groceries' store. Kind of a crapshoot.

## Topics for the Future

_I type on computers for a living, why should I care about writing on paper?_

This will get its own post. My teaser response is that paper writing is a power tool for self-management, idea development, and mental health.

_What about paper / notebooks?_

Also its own post, but I will tease with "1:√2".
