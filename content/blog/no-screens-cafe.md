---
title: "No Screens Cafe"
date: 2023-05-21T11:44:53-07:00
tags: ["cafe", "no screens cafe"]
draft: false
---

# No Screens Cafe

At the No Screens Cafe, you can enjoy coffee, tea, and scones. You can make conversation with neighbors, friends, and folks you've never met. You can have a first date or a 101st date. You can read and write. You can make music. You can stay here as long as you wish.

You just can't use any screens. No phones, tablets, laptops, "smart" watches, or other things with a screen. Leave them at home, or failing that, leave them shut off in your bag. If we see your screen out, you'll no longer be welcome.

Do we have other rules? No earphones. No ringtones. You can listen to your inner voice, your fellow patrons, and the birds outside. Maybe you'll have some new ideas.

You can talk about whatever you want. Including screens! You can argue with us about the rules. We're used to it. Why no screens? Come visit and find out.

E-book readers are screens, sorry. You're welcome to read a regular book. We have whole shelves of them.

Yes, there's a bathroom. Inside it, a sign says "Big Brother is Watching". We don't have cameras or anything creepy like that. But we can't say "God is watching". Because we killed him.

The No Screens Cafe accepts cash only. You can get this from your bank! It's not illegal yet.

We're open early. We're open late. Stop by after dinner.

We don't have any parking for cars. Cars kill innocent pedestrians and emit toxic gas. Cars ruin cities. But we do have a bike rack. When is the last time you got on a bike?

We're also near a bus stop. Which bus? No idea. Ask someone who rides the bus.

You're welcome to review us on Yelp. Just please don't do it here.

Sorry, the No Screens Cafe doesn't have wi-fi. Maybe try the Cowork Cafe next door.