---
title: "Effective Technologist Book Club"
date: 2024-01-12T22:00:00-07:00
tags: []
draft: false
---

![A cozy, well-lit room with a group of 8 people engaged in a book club meetup. The attendees are a diverse group of technologists gathered around a large table. They are in the midst of an animated conversation, with some holding books or tablets. The room features comfortable seating, warm lighting, and bookshelves filled with various books in the background, creating an inviting and intellectual atmosphere.](../img/effective-technologist-book-club/book-club.jpg)

Book clubs for professional development are not new, but nothing like this exists at my employer or in my city, so I guess I'll start one.

_What does "Effective" mean?_

Borrowing from another group [^tfp], I'll say _whatever it means to you_. [^forme]

_And "Technologist"?_

Someone who builds or wields technology as a career. It's a big tent including software and systems engineers, hardware engineers, UX practitioners, project managers, researchers, students, data scientists and more.

_What's the format?_

Other book clubs are [intensely organized](https://web.archive.org/web/20240113033139/https://medium.com/flatiron-labs/7-secrets-for-scaling-a-technical-book-club-6f15d06db063), but this one is simple. I suggest the following, very open to modification.

We can have monthly video meetups [^remote], scheduled via [When2meet](https://www.when2meet.com) or equivalent, hosted on [Jitsi Meet](https://meet.jit.si) or equivalent. [^jitsi] We'll do two things:

1. Choose the next month's book via [ranked choice voting](https://en.m.wikipedia.org/wiki/Ranked_voting).
2. Spend 1-2 hours [^duration] talking about the previous month's book. What did we learn from it? How do we try to apply it in our work? Where is the book wrong?

Between meetings we can coordinate via email list, because the last thing we all need is more chat notifications.

_What kind of books?_

Primarily (but not exclusively?) non-fiction that might grow one's effectiveness and wisdom as a technologist. Topics might include socio-technical systems, software engineering, productivity, leadership, communication, incentives in organizations, case studies, and biographies. [^blog]

We can learn about successful projects, crashed planes, and how to work with the most complex systems of all: human minds, and the organizations and societies they create.

_That's vague, give me examples._

I've read only part of each of these, but I think they fit the theme.

- [_The Effective Engineer_](https://www.goodreads.com/en/book/show/25238425) by Edmond Lau
- [_Nonviolent Communication_](https://www.nonviolentcommunication.com/product/nvc/) by Marshall Rosenberg
- [_A World Without Email_](https://calnewport.com/a-world-without-email/) by Cal Newport
- [_The Soul of a New Machine_](https://en.wikipedia.org/wiki/The_Soul_of_a_New_Machine) by Tracy Kidder
- [_Kill it with Fire_](https://nostarch.com/kill-it-fire) by Marianne Bellotti

_I'm not a programmer, do I belong?_

Yes!

On one hand, everyone's work is adjacent to code in some way, and growing one's code literacy is a way to become more effective. On the other hand, a lot of technologists don't work with much code directly, and I want to include you.

I imagine some books will be code-adjacent, and a few might be code-focused, but many won't be either of those. This is not a programming book club per se.

_Does this book club scale?_

It doesn't, because I don't want to spend much time organizing it, and a reasonable-length meetup allows maybe 12 people to hear each others' perspectives. If it grows larger, maybe we splinter.

_What's next?_

**Send me a message:** bookclub at cmart dot today. Tell me what you like and dislike about this idea. What variation of it would you most want to join?

I have about 3 people interested so far. Let's find our alignment and grow something new.

If something like this already exists, and we should join that instead of re-inventing, please tell me that as well.

[^forme]: For me, it means things like setting ambitious-yet-achievable goals, making important contributions to the teams and projects I join, increasing my motivation, gaining better control of my attention, working and communicating well with others, exchanging mentorship, and building my career proactively.

[^remote]: Yes, the DALL-E pic above shows an in-person group. But I'd like to include people who don't live nearby, and hybrid meetups are kind of awkward.

[^jitsi]: It's kinda like Zoom, works great, but it's non-proprietary software.

[^blog]: I imagine that longer essays and blog posts are also fair game. As long as everyone can reliably get their hands on a copy.

[^duration]: I think we should try to give everyone a chance to share their perspective within an hour. After that, staying to talk longer is cool, and leaving early is also cool.

[^tfp]: [Tucson Functional Programmers](https://www.meetup.com/Tucson-Functional-Programmers)