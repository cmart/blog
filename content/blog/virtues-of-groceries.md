---
title: "The Virtues of Groceries"
date: 2024-08-31T22:30:00-07:00
tags: ["groceries", "frugality", "eating", "food"]
draft: false
---

Warnings:

- This topic, personal eating habits, is tired. Anything I could say, a different nerd has said better.
- I currently eat meat but try to avoid eating mammals, which I learned today is called [mafism](https://english.stackexchange.com/questions/536069/single-word-for-someone-who-does-not-eat-mammals-meat).
- I'm a mid-30s knowledge worker who lives with a partner. We share some meals spontaneously but don't plan around it, each keeping our own separate food. My experience won't generalize if your situation is _very_ different.

Returning from a long and [tasty](https://en.wikipedia.org/wiki/Flammekueche) vacation, my partner and I made an unusually serious commitment to each other: **no restaurant, cafe, or take-out food for a whole month.** We allowed one narrow exception, non-caloric beverages (e.g. black tea or coffee) in a social setting. That aside, everything we ate had to come from home (and ultimately from a grocery store). For this impatient millenial, getting _everything_ I eat from a grocery store was a big change of routine.

!["food at home" meme. Image is a child wearing a chef's hat, holding an apple and a block of cheese, making an incredulous face. Caption is "When you want mcdonalds but ya mom says there's food at home".](../img/virtues-of-groceries/food-at-home.jpg)

My routine leading up to this:

- Before 2020
  - Lost pre-history.
- 2020-2021
  - Scary virus.
  - Eat 95% at home, 5% take-out.
  - Learn to make good pizza and yogurt from scratch (later forgot).
- 2021-2023
  - Start working from cafes.
  - Often spend $20-40 USD at whatever cafes I happened to visit that day.
  - Often get take-out for dinner (or in a pinch, Taco Bell).
  - Slowly forget how to make food at home.
- 2024 March to July
  - Start [a tiny co-working space](https://tucsoncode.works).
  - Start habit of bringing breakfast to the office from home, but frequently, lunch from a restaurant.
- 2024 second half of July
  - Eat at restaurants across Europe 2-3 times per day.

Today, the last day of August, is the last day of our pact. I have stuck with it. Reflection follows.

## Why did this?

It became salient that food from a restaurant or cafe is:

- _several times more expensive_ than food from a grocery store!
- less healthy because we don't control the ratio of macronutrients, or the sugar and salt going into it.
- often the wrong portion size: a tiny pastry at a cafe when you're hangry, or (more often) a Really Big Bowl of noodles that you feel irrationally pressured to finish.
- often a [super-stimulus](https://en.wikipedia.org/wiki/Supernormal_stimulus), which I want to reduce exposure to.
- in take-out form, an outsized consumer of single-use plastic that [probably doesn't get recycled](https://www.npr.org/2022/10/24/1131131088/recycling-plastic-is-practically-impossible-and-the-problem-is-getting-worse) no matter which bin it ends up in.
- in some ways less convenient than groceries at home, because each meal requires travel and/or waiting.

(Neither of us were trying to lose weight, and mine probably didn't change much over the month.)

## What did I learn?

First, I must keep ahead with planning.

With Restaurant Brain™, I can go about my day completely forgetting that food exists. Then I get hungry, go to a place, and tasty food magically appears. (Repeat several hours later, tomorrow, etc.)

With Grocery Life™, I cannot do this, because (at least where I live) a grocery trip followed by cooking takes _too long_ when I'm already hungry. A _well-organized_ grocery trip takes longer still.

So, to win at grocery life requires _thinking_ a week (or more) ahead. What will I want to eat? What do I know how to make?

The prep work takes time. I've spent 3+ hours each weekend buying groceries and pre-cooking a bunch of stuff for the coming week. I don't try to "meal prep" in the sense of pre-portioning meals, but I _will_ pre-:

- Grill a bunch of chicken thighs and some salmon
- Cook some brown rice, farro, and/or lentils
- Maybe make a batch of Nutrient Slop (recipe below)
- Maybe bake a loaf of, e.g., banana bread (from a box with instructions)

If I'll spend much of a given day away from home, I also need to portion and pack whatever meals I'll want to eat. This takes probably 15-20 minutes each weekday morning.

The reward that this system delivers, for all the work to run it? **Tasty, healthy, appropriately-portioned, inexpensive meals, ready with minimal assembly and heating, whenever I get hungry.** That sure beats take-out.

What else? I'll get several kinds of fruit. Strawberries, cherries, bananas. I generally eat some at the end of both breakfast and lunch. It's (usually) sweet enough to stop me from craving junk food.

## What I eat in a typical day?

- Breakfast at the office
  - Hot oatmeal with chicken sausage and chopped apple stirred in.
  - A handful of strawberries on the side.
- Lunch at the office
  - A pre-made burrito (chicken, corn, beans, rice), microwaved and then finished in the toaster oven.
  - A handful of cherries on the side.
- Dinner at home
  - A big bowl containing
    - A hunk of fish or chicken (cooked the previous weekend).
    - Brown rice.
    - Half of a bag salad kit, with the dressing all over everything.
- Dessert / evening snack
  - Granola with oat milk and chopped strawberries.

## Accidental Love Letter to Trader Joe's

I believe that Trader Joe's, more than other grocery stores, reduces the effort of eating tasty, sort-of-healthy food, for sort-of-cheap. (That said, you save a bunch of money if you buy your meat from a variant of the Kroger Empire, or Wal-Mart or Costco, I guess.)

Try the TJs bag salad kits that include dressing and some nuts or crunchy bits. They make it easy to add a lot of plants to a meal. Just watch for added sugar in the dressing. The salad kit alone typically has more dressing than I need, but if I'm eating it with salmon and rice, I welcome the extra dressing to season the rice.

Try the pre-made burritos, refrigerated section, under $5 each. Here is a nano-review of each variant.

- Pollo asado: +2 tasty, good salsa, fiber from beans.
- Vegetable samosa: +2 amazing but usually out of stock here. :(
- Beans and rice: 0 filling, cheesy, but boring texture.
- Carnitas with salsa verde: 0 somewhat tasty but I try to avoid pork.
- Chicken sausage breakfast: 0 tasty but needs salsa, cholesterol from eggs.
- Cheeseburger: -1 very tasty but not that great for you.
- Spinach & Feta Egg White: -1 not much flavor.
- Turkey & Sweet Potato: appears to exist online but NEVER seen in store.
- Jerk Chicken: ditto.

"Organic Rolled Oats with Ancient Grains and Seeds" forms the base of my breakfast on weekdays.

When I cook a bunch of meat, I'll glop on some Soy Vay (or "Soyaki", the TJ equivalent), let it soak for a few minutes, then grill it outside.

A good "ah crap we're out of everything" dinner that keeps forever: a frozen bag of vegetable fried rice and a frozen bag of shrimp. (TJs has both.) Sautee the fried rice while the shrimp thaws in a bowl under running water, then stir the shrimp in.

## Do I have food storage opinions?

These aren't new opinions, but yes, the [IKEA 365+ food containers](https://www.ikea.com/us/en/p/ikea-365-food-container-with-lid-rectangular-glass-plastic-s89567061/) are superior. They come in multiple materials and sizes. For the 34 oz and 61 oz sizes, all of the lids are cross-compatible. The latch-on lids are pretty leakproof and I've found the latches are durable.

Get a bunch of 34 oz and 61 oz glass containers for home. Get several 34 oz metal and plastic containers to take places. Also get a [set of inserts](https://www.ikea.com/us/en/p/ikea-365-insert-for-food-container-set-of-3-dark-blue-60500896/) to keep meal components separated in transit.

Just be aware that when you drop a glass one, it will shatter violently. I prefer to travel with the metal ones (or plastic if I'll need to microwave it).

## What about Soylent? Other meal replacement products?

From 2015 to 2017, I ~~ate~~ drank a _lot_ of Soylent, mixed from powder, and it was great. There was a week when I consumed only Soylent and no other food; that was great too. Eventually, I mostly stopped because it started to hurt my stomach. I don't know why.

I had a bottle of [yfood](https://yfood.com/en-eu) from a vending machine in Germany and it was _great_. I see they sell powdered mix too, but unfortunately none of it is available in the USA.

I've eaten a lot of the previous version of [MealSquares](https://mealsquares.com), and I'm waiting for my first box of version 2.0.

## cmart's Nutrient Slop recipe

[![4-panel collage of preparing cmart nutrient slop. Top-left: sauteeing vegetables. Top-right: Mixing ingredients in bowl. Bottom-right: Ingredients fully mixed in bowl. Bottom-left: slop in glass food storage containers.](../img/virtues-of-groceries/cmart-nutrient-slop-small.jpg)](../img/virtues-of-groceries/cmart-nutrient-slop.jpg)

All quantities are approximate. Make your slop different than mine!

- Pre-cook:
  - Some brown rice (8 ounces or 225g).
  - Some lentils (6 ounces or 170g).
- Chop up and saute together:
  - 2 bell peppers.
  - 2 onions.
  - A bunch of mushrooms (maybe discard the stems).
  - A carrot.
  - Maybe some kale?
  - A few cloves of garlic (or just throw it in raw later).
- Then, assemble and stir in a big mixing bowl:
  - The cooked rice and lentils.
  - Everything you just sauteed.
  - A fistful of chopped-up cheese (I like _goat cheddar_).
  - Glop in some olive oil for fat.
  - Seasonings to taste:
    - Rice vinegar.
    - A little fish sauce (or failing that, salt).
    - Ground pepper.
- Chill immediately.

It keeps for a week. I'll slop some into a bowl, eat it cold or slightly warmed. As a small meal it's fine on its own. As a bigger meal, I'll put a chicken thigh on top.

## Conclusion

I might keep going!? It's been fun to build and optimize this whole system. I'm saving a lot of money.

_Cities need more open-to-the-public, BYO-friendly cafeterias_ (ideally with microwave ovens). This would provide social opportunities while supporting health and frugality. Something like the student union at a university, or the food court at a hospital, in every neighborhood.