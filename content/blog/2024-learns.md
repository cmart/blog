---
title: "Some Things I Learned in 2024"
date: 2025-01-08T23:00:00-07:00
tags: []
draft: false
---

**Learned:** if you're working remotely, but you want to bring back the daily experience of going to an office with some friendly people, maybe don't start [a coworking space](https://tucsoncode.works). It probably won't work. You can recruit a half-dozen friends who like you, and they'll show up occasionally, but not daily. You can offer Aeron chairs and tasty snacks. You can make it more-or-less free. Same result.

![A photo of me sitting alone at my desk.](../img/2024/cowork-alone-small.jpg)

Nearly every coworking space fails within a few years, even without a pandemic. In my mid-sized US city: [dead](https://www.yelp.com/biz/common-workspace-tucson), [dead](https://www.yelp.com/biz/colab-workspace-tucson-2), [dead](https://www.yelp.com/biz/connect-coworking-tucson), [dead](https://www.bringscoworking.com/). All of those places were great places to work (I either know firsthand or heard from friends).

But what if you have a really energetic community of like-minded folks working on related projects? And $1.4m per year to spend? And you're not even trying to make money? [It sounds like that didn't work either.](https://forum.effectivealtruism.org/posts/rsnrpvKofps5Py7di/shutting-down-the-lightcone-offices)

The enduring counter-examples that I can think of tend to be either [maker spaces for hardware projects](https://ithacagenerator.org) or [a placeholder use for commercial real estate](https://www.wework.com/) that somehow shambles on through bankruptcy. I know of [just one](https://temescalworks.com) 'pure', independent coworking space that seems to be doing great. (If you've come across others, please write to [coworking-spaces-that-havent-failed@cmart.today](mailto:coworking-spaces-that-havent-failed@cmart.today).)

I just want to leave home every morning to work at a nice place around smart and friendly people. I think there is nuance we're collectively missing between corporate RTO mandates and the predominant revealed preference to WFH. 🤷

**Learned:** [the Jhanas are extremely real](https://asteriskmag.com/issues/06/manufacturing-bliss) and [surprisingly accessible](https://www.lionsroar.com/entering-the-jhanas) to people who are even mildly practiced with meditation. You don't need to believe that it will work at all (I sure didn't).

If something like Jhana practice were legible to the medical profession, I imagine it might be more important than SSRIs, maybe more important than all psychiatric medications combined. Unfortunately the patent expired 26 centuries ago, so there's no market incentive to do the clinical trials for FDA approval. 🙂

This one is a contender for the most surprising thing I've learned about in adulthood, and the weirdest thing to tell people about (_much_ weirder than bidets).

**Forgot:** by default, conferences and work travel are a quadruple-whammy of poor diet, poor sleep, shouting, and virus exposure. It's no wonder people return feeling wrecked. _But it doesn't have to be this way._ Attendees can be selective about their consumption (you don't need to eat all the fries), and seek smaller conversational groups. Event organizers can avoid crowded receptions and poster sessions, arrange smaller tables for meals, and ease jet lag with a later start to morning sessions.

**Forgot:** pandemic aside (and even altruism aside), a well-fitted N95 is so worthwhile in crowded places where sick people end up anyway. Right now I'm coming off a week of some flu. I'd have accepted _so much_ mask-related annoyance in order to not finish the holidays in a feverish haze. It's illustrative to compare the respiratory route of disease transmission with the fecal-oral route. We've closed off the latter in the developed world, with sanitary supply chains for food and drinking water. Raw-breathing an airport security line with 1000 other people is kinda like dipping your cup in the Ganges.

**Forgot:** alcohol consumption (even a single drink) is basically never worthwhile for me. My sleep suffers and I get depressed on the following day(s). Now that my watch tracks my sleep quality, this pattern stares me in the face. It's still too weird to say "no thanks, I don't drink" as someone who is neither religious nor recovering from addiction, so I _do_ occasionally drink, but I should get over this and say it anyway.

**Learned:** I tried modest amounts of coffee after not touching it for ~13 years. It still makes me uncomfortable in my skin. Fortunately, tea continues to bring joy and warmth.

**Learned:** Garmin watches work quite well 'airgapped' with no bluetooth connection or phone app at all. You get the health tracking benefits of a smart watch without any tech company collecting your personal data. (In contrast to Apple's walled-garden approach, you can even mount them as a USB mass storage device, and they expose a filesystem containing things like your activity history.)

Here, watch me get real sick.

[![A small multiple collage of graphs from my watch showing health stats when I was sick.](../img/2024/watch-sick-small.jpg)](../img/2024/watch-sick.jpg)

**Learned:** men's pants technology has evolved. I don't want to shill for specific brands, but one can have pants that are essentially office-appropriate chinos with an elastic drawstring waistband that feels like pajamas. (Maybe spreading the word will save coworking spaces from obscurity?)

**Learned:** the nicest surprise from my first time in Europe was the ubiquity of neighborhood grocery stores. You have regular apartment buildings with a Lidl or Jumbo on the ground floor, scattered all over a city. The result is that you can take a short walk to obtain healthy groceries at supermarket prices. It's whatever the opposite of a food desert is. Most of the USA made this illegal with zoning laws, so instead we get supermarkets with acres of car parking, far from everything else in our lives.

(Another thing Europeans get to have: real butter in shelf-stable foods like granola bars, which makes them tastier.)

**Learned:** we didn't really have numerical graphics (charts, plots, etc) until the late 19th century, when William Playfair invented the line chart, bar chart, and (regrettably?) pie chart. Before then, we had tables of numbers, but we didn't have charts that use lengths of lines and shapes on a page to represent numerical values.

This feels like a really important thing for humanity to sleep on for 3 centuries after the Renaissance and Scientific Revolution. What are we still sleeping on?

**Learned (but sort of already knew)**: I think Dwarkesh Patel [said it well](https://www.dwarkeshpatel.com/p/notes-on-china).

> What's lacking in life is not time. It's focus. If you're working on what matters, you can advance leaps and bounds in 8 hours. And if you're just clearing the slog, you can spend a lifetime staying in the same place.

Happy new year, internet!
