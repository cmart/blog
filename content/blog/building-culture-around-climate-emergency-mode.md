---
title: "Building Culture Around Climate Emergency Mode"
date: 2022-07-24T22:49:07-07:00
tags: []
draft: false
---

_This is a draft blog post, I would like to hear how I am wrong, so I can revise before sharing this more widely. Please email chris at c-mart dot in._

We only have one spaceship, we are irreversibly damaging its life support system, but the sum of our actions can slow the damage a lot.

I am not an expert on the climate, but I have a vaguely intuitive sense of thermodynamics and human incentives.
When Peter Kalmus at NASA's Jet Propulsion Lab [talked about Climate Emergency Mode](https://cafe.com/stay-tuned/climate-emergency-mode-with-peter-kalmus/) on a not-climate-related podcast, I found it pretty credible.
His interview with Preet Bharara starts 17 minutes into the audio, and there's a transcript on the website.

In 2006, [Al Gore's slide deck](https://en.wikipedia.org/wiki/An_Inconvenient_Truth) did not hit home for me.
I understood that someday it will be warmer outside.
But at the time, I was a teenager, I wanted to learn to drive a car.
I've since followed climate-related news, but it didn't really change my personal habits.
In 2019, I drove a 33-foot-long diesel motorhome 7000 miles.
So far this year, I have made three round-trip transcontinental journeys.
One by land and two by air, all of it petroleum-powered.

But two months ago, Peter Kalmus' interview planted a seed in whatever part of me makes actual life decisions.
There are several important takeaways.
My biggest is this (and Peter may not entirely agree):
**In our fossil fuel-powered economy, we all need to cut back our consumption right now.**

To the extent that we want humanity to have a less-bad future, we need to sharply reduce our fossil-fuel-powered activities.
This currently includes most forms of travel and consumption of products.
As long as 'going to X', 'buying Y', or 'doing Z' will cause carbon in the ground to enter the air, we should be asking ourselves if we can deal without it.

I can already hear you (and maybe also Peter) saying "But shifting the onus onto our personal choices would absolve fossil fuel companies and their investors of responsibility, when they should rightly bear it!"
I just don't believe anyone can make them bear that responsibility. 
ExxonMobil's investors cannot revive the [couple thousand people who died of heat stroke in Spain and Portugal last week](https://www.axios.com/2022/07/18/heat-wave-europe-death-toll). They cannot suck 70 PPM of CO2 back out of the atmosphere.
And ExxonMobil cannot stop doing the still-legal activities that maximize shareholder value: extracting, refining, and selling petroleum products.

Optimally, leaders of the most carbon-emitting countries would sign onto something like the [Fossil Fuel Non-Proliferation Treaty](https://en.wikipedia.org/wiki/Fossil_Fuel_Non-Proliferation_Treaty_Initiative), then wield regulatory carrot and stick to make ExxonMobil illegal and steer the global economy away from fossil fuels.
Unfortunately, we don't have anything like that now. [^1]

So what do we have?
"Net zero by year X", an [optimistic distraction](https://en.wikipedia.org/wiki/Carbon_neutrality#Criticism) that excuses the status quo.
Net Zero by 2050 (or whenever) gives current leaders a pass to finish their careers and retire before anything _different_ and _hard_ has to be done.
The premise of "carbon neutrality" is often a lie, because [carbon offsets are a sketchy shell game](https://www.youtube.com/watch?v=AW3gaelBypY).
In all of their popular forms, offsets likely fail at their One Job.
When you see talk of carbon neutrality, ask (1) how much fossil fuels are you still burning, and (2) how will you get all that carbon back out of the atmosphere?
To do (2) directly and without hand-wavy bullshit is currently [very hard and expensive](https://en.wikipedia.org/wiki/Direct_air_capture#Cost).

We also have technologies like electric vehicles.
These are incremental mitigations for burning fossil fuels, but not even close to a complete fix [^2]. I believe they are getting too much mindshare because they cannot the needle as much as we must (and can!) move it now.

Here is a thing that we have: **the sum of everyone's personal choice to travel and consume less.**
This is a really big lever and it's [definitely within our reach](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7883727/).
When we stayed home in the early days of CoViD-19, we accidentally pulled the lever back _so hard_ that [crude oil prices became negative](https://www.marketwatch.com/story/oil-prices-went-negative-a-year-ago-heres-what-traders-have-learned-since-11618863839).
We can pull it back again, but without the sickness and social isolation and fear.
We need to continue pulling it back, as much as we can collectively abide, for as long as our economy is mostly powered by fossil fuels.
For many of us, that could mean for rest of our lives.
This is the collective action that feels under-emphasized to me right now, and the opportunity to build culture around.

**How can you have an awesome life that slows the rate of greenhouse gas emissions?**

**How can people raise great families without making the climate worse?**

Right now, this certainly looks like cutting back on travel and car-enabled activities, perhaps even when the car is electric.
But it also looks like using our personal resources in more efficient ways.
Maybe we need the sort of household economic culture that came from the great depression and World War II: conserve everything and spend less money.

For some people, this might mean moving from suburbs and exurbs into cities (the general opposite of CoViD-19-era migration patterns). Virtual presence technologies, tired as we may be of them, would see more use to support long-distance collaboration, family ties, and friendships.

I think we can build new ethics and culture around this less-consumptive, climate-friendlier living.
We need to make it matter to those who wield power.
Make it intersectional, but also make it multi-partisan, acknowledging that some fraction of people won't participate.
But make it part of our art and music.
[Make it fun](https://sprocketpodcast.blubrry.com/), to the extent that we can.
Make it make sense to young people in small towns and developing countries.
Building culture is hard work, but have you heard how hard it is to deal with climate change?

It matters a lot how we reason about our choices.
If you take emissions-reducing actions to their natural extreme, you will end up at "all humans must be destroyed", or perhaps the milder ["people should stop having children"](https://en.wikipedia.org/wiki/File:Wynes_Nicholas_CO2_emissions_savings.svg).
But the future of humanity requires new humans!
It also requires a critical mass of support from existing humans who are trying to imagine their grandkids having a good time.
I think the right way to frame it is making decisions for the greatest human benefit, but factoring in the long-term climate cost.
The question to ask "on the margin" is: which course of action will yield the most units of human flourishing per units of carbon emission?
People are not used to reasoning like that, but I think we gotta learn how.

I also think it's generally important to avoid casting judgment and shame when others continue to consume fossil fuels, for two reasons.
The first reason is that change is hard. Entire families spend 15+ hours per week driving around.
They've got places to be all over, and your demand that they stop now will only annoy them.

The second reason is that it can backfire. If you [flight shame](https://en.wikipedia.org/wiki/Flight_shame) people who don't share your politics, they will embrace it.
Your flight shame will fuel their flight pride.
They will roll coal from lifted trucks with stickers that say "I identify as a Prius".
Maybe some of this is an inevitable consequence of building culture, and it mustn't discourage us from doing it anyway.

---

Anyway, this is my takeaway a couple of months after hearing Peter's interview.
I now experience hesitation about activities like making an extra car trip to get take-out food, or buying things that I don't really need.
This guilt feels healthy, so I am treating it with legitimacy, but I have personal things to resolve.

For one, my entire blood family lives on the other side of the continent, as do good friends.
I care about them a lot and I want to visit occasionally, but I'm not going to move back there.

For two, I love riding bicycles, but it cuts both ways.
I especially like mountain biking, which means burning most of a gallon of gas to get to a trailhead, every time I want to do it.

For three, my last road trip resulted in my car smashing into a deer and left for months to wait for repairs, 2500 miles from home.
Soon it will be fixed.
I really like this car, but when you include an outbound flight, the trip to bring it back here will emit literally the car's weight in CO2.
All so I can use it again, to emit more CO2.

I'll leave on this note. Do you remember [350.org](https://en.wikipedia.org/wiki/350.org), the big organizing effort back in 2007? We're now at 421. The line keeps going up, faster. I want to help slow it down. Do you want to help too?

## Footnotes

[^1]: I don't know the right kind of government or economy to help climate change.
Our government in the USA is developing helpful technologies via the [national labs](https://en.wikipedia.org/wiki/United_States_Department_of_Energy_national_laboratories), but the USA [doesn't really have a coherent or credible strategy](https://en.wikipedia.org/wiki/Climate_change_policy_of_the_United_States) to reduce emissions.
Also, our highest level of government [just un-recognized women's right to reproductive autonomy](https://en.wikipedia.org/wiki/Dobbs_v._Jackson_Women%27s_Health_Organization), after [recognizing this right](https://en.wikipedia.org/wiki/Roe_v._Wade) half a century ago.
This reversal opposes efforts toward gender equality, and also public sentiment.
The ostensibly-more-climate-focused-but-also-women's-rights-focused political party is understandably pouring their energy into damage control, and they cannot seem to get out of their own way.
There are [options on the table](https://www.bbc.com/news/world-us-canada-62284378) to increase government's ability to reduce carbon emissions, but these are not yet used and they don't feel like part of the public conversation to me.

[^2]: On the margin, battery-electric vehicles are better for the world than fossil fuel-burning cars.
But electric cars don't help the fact that roads and parking lots are ALL still made by fossil fuel-burning machinery, and the more people drive, the more of all that needs to be periodically rebuilt and expanded.
Also, the vast mining, refining, shipping, and manufacturing supply chain to build electric cars (and their batteries and consumable parts) is still mostly powered by fossil fuels.
So, electric cars are good, but less vehicles and less vehicle use is better still.
