---
title: "Request for Comment: Works in Progress Retreat"
date: 2023-09-27T01:29:00-07:00
tags: []
draft: false
---

## An invitation for software developers, makers, and similar creators

Is there a project that you'd love to work on, but work and life get in the way? Join a week-long retreat to incubate or evolve your project in the energizing company of others doing the same. My goal is to enable our self-actualization. We'll hack on our projects together in an inviting and comfortable space, share meals and laughs, and at the end, take turns presenting our creations to the group. Along the way, we may collect new friends, new ideas, and/or new career directions. I'm not trying to make money: you pay a nominal fee to cover the cost of the venue, and arrange your own travel and accommodation nearby.

The name _Works in Progress_ refers to both the unfinished nature of our projects, and the broader idea of [progress](https://en.m.wikipedia.org/wiki/Progress) that our projects enable.

---

I'd like to host an event like this, but haven't fully planned it yet. I'm putting this out to collect feedback from people who may want to join. **If you might be interested, I want to hear from you!** Would this kind of event work for you? What would make it even better? Send a message to `email at cmart dot today`, and tell me whether it's okay to put your message on my blog.

## What do you know about retreats? Why are you doing this?

Most years since 2017, I've co-organized a small yearly retreat around the open-source project that I co-maintain, [Exosphere](https://exosphere.app). The project got started this way, and the retreat aspect has been really special at times. We've tweaked the event format as Exosphere has grown, so I've seen some of what works well and what doesn't.

I'm doing this to provide a [third place](https://en.m.wikipedia.org/wiki/Third_place) for other projects to start and grow, as Exosphere did. It's been a highlight of my life, and I want to facilitate this for others, too.

## What is and isn't a suitable project? Can I work remotely for my job?

The most suitable project is _the thing you're most personally excited to work on_, and the most suitable goal is what feels _ambitious but achievable in a week_.

- It's fine if you want to make it open-source, or not.
- It's fine if you're turning it into a business, or not.
- It's fine if someone else started the project, and you're contributing to it.
- If the project already exists, have a goal of _what_ you want to accomplish for it during the retreat (beyond just "work on the project").
- It's fine if this is a ["20%" project](https://en.m.wikipedia.org/wiki/Side_project_time) for your job.
  - In fact, you can totally pitch this to your employer as "I want to go mostly dark for a week to deliver this great new thing for us". Maybe they'll pay your travel expenses!

Failing all that, it _won't_ be okay to spend most of the retreat working your daily grind of chats, meetings, tickets, and so forth. Works in Progress is for people who can mostly step aside from the noise of their day job for a week, whether this means taking vacation or striking a compromise with your employer (along the lines of 'I'll respond to emails and chats a couple of times per day but otherwise keep them off'). An hour or two per day is fine, but this degrades the third place into a second place. [^dayjobs]

[^dayjobs]: To be fair, at last year's Exosphere retreat we explicitly said it was cool to just work your day job. I know some of you will read this, so please don't interpret this as a complaint! :)

If too many people express interest in attending, I'll probably give preference to projects that are either open-source or serve the (broadly-defined) public good -- whatever the opposite of [this](http://www.stupidhackathon.com/) is.

## What about a group project?

If you and a handful colleague(s)/friend(s) want to join together and work on the same project, yes, that's rad.

## I don't know what to work on?

I can [give you something to do](https://gitlab.com/exosphere/exosphere/-/issues), but I think it's even better if you find and grow your own thing. Here are [a bunch of internet people's opinions](https://news.ycombinator.com/item?id=36550615) on how to find (and do) great work.

## This sounds like a hackathon?

I wouldn't call it that. The hackathon format tends to encourage sleep deprivation and working very intensely to cob together something that barely works. Works in Progress is not frenetic. Take walks and rest well.

## Where and when?

Not sure! Some wants, in decreasing order of importance:

- An affordable small event venue. It should have nice tables and comfy chairs, natural light, maybe also a living room. It shouldn't feel too corporate or flashy. It should basically be ours for the week (including at night).
- Walking distance to:
    - Several Airbnb / hotel / hostel options.
    - Good food, coffee, etc.
- Near a well-served airport.
- Not a super expensive city to stay and eat in.
- Nice weather for the time of year, near a park or other natural space.

Maybe we do this in winter or early spring 2024. That leaves a few months to plan.

## But like, where in the world?

I live in Tucson, Arizona. We've had great retreats in a beautiful place nearby[^bz]. It doesn't _have_ to be in the Southwestern USA though. We could choose a place that minimizes the total amount of jet lag. This could be somewhere like St. Louis (if the cohort is mostly USA-based), or somewhere kinda like Boston but cheaper (if a bunch of folks also come from Europe).

[^bz]: Sorry, I don't want to say exactly where because it's tiny and already pressured by tourism.

## How do we stay present and focused?

I would _encourage_ folks to enter the retreat space with chat apps closed and message notifications shut off, phone on do-not-disturb mode, and so forth. I'd also ask folks to step outside (or go back to a hotel room) if they need to get on a call. Some mindfulness around interruptions from the outside will help us make the most of the week.

I'd probably set up a discussion board for coordination and logistics, but discourage a retreat-specific real-time chat. We're trying to be present, not build yet another hyperactive hive mind.

## These sound like guidelines, are there any hard rules?

Probably a lightweight code of conduct that boils down to "treat each other with respect, else you'll no longer be welcome". Probably don't try to monetize other attendees (e.g. by selling their contact information to recruiters). This is probably a recruiter-free zone.

I can't think of anything else right now.

## What's the Schedule?

Approximately:

- Sunday
    - Arrive and check into your accommodation.
    - Evening social meetup.
- Monday morning
    - We each give a 5-minute lightning talk to the group about what we want to build during the week.
- Mon-Thurs late afternoons
    - Maybe a daily tech talk? (See section for this)
- Friday afternoon/evening
    - We each give a 10-minute live demo on what we've created!
- Saturday
    - Go home.

Most of the week is intentionally unstructured. Folks will mostly hack on their projects in the event space, but also form spontaneous break-out groups for cafe/meal breaks and nature walks.

## How big?

Probably 12-15 people? Fewer than 10 feels not worth organizing for. 20 feels chaotic to have in the same room (and it will take too long for everyone to present to the group).

If there's a _lot_ of interest, we should have multiple events (and I'll invite others to step up and organize them).

## Why not a residence program or rent a hotel block?

Mostly to reduce the cost, risk, and hassle of organizing. Also, different people will want different accommodations, and in a city with multiple options nearby, everyone can pursue their preference. Maybe we provide a discussion board for room-sharing coordination.[^room-sharing]

[^room-sharing]: In the past we (as organizers) have negotiated a hotel group rate, organized people sharing rooms, and subsidized the hotel stay for folks who need it. For a larger group this can be a lot of work (and costly if people forget to pay).

## Presentations?

I could imagine a few lectures (on a wide variety of topics) to stimulate some great dinner conversations. It would be _extra cool_ if attendees themselves give the talks -- this would be in the spirit of [Alex Miller's call to action](https://youtu.be/suv76aL0NrA?t=2683) at the end of the final [Strange Loop](https://www.thestrangeloop.com) conference: to host conferences in your own city, with local speaking talent. That said, preparing for even a low-stakes presentation can be all-consuming, so it would certainly delay the speaker's progress on whatever they came to work on.

So, here's my idea of a compromise: Monday through Thursday (maybe at 4:30 PM), we watch a pre-recorded conference talk together (perhaps from Strange Loop). Attendees can suggest and vote on these talks ahead of time, and we watch the four talks that get the most votes.

## Privacy or showcase?

For a variety of reasons, some people may want a semi-private space to work on their project. Maybe their employer isn't friendly to personal software projects built on personal time. Conversely, some people will want to put their project in front of the world, and welcome help doing so. I think a small enough retreat can provide both of these. We can do this:

- Provide a few options on the application form, ranging from "I'm working in the open and welcome free publicity" to "my project and attendance is private and I don't want anyone outside the retreat to know about it".
- Require all attendees to agree to honor requests for privacy, not talking about certain projects or attendees (or sharing photos of them, etc) outside the retreat.
- Before the retreat, make the cohort aware which projects/attendees need to remain private.
- After the retreat, we feature the public projects on a web site.

This is essentially a handshake agreement, not legally binding, and it's possible that (despite best intentions) we end up with a dishonest attendee who will tweet about everything they see and hear. So, if that happening to you would be really bad, maybe Works in Progress isn't a good fit for you.

## I have a family, I could get away for a day or two but not a week?

I know this won't work for everyone, but it's totally an option to bring your family to the retreat city. You could spend some of the day hacking with us, and some of the day sight-seeing with them. I'd probably ask that you commit to at least 5 hours a day to be present and engaged at the retreat. (The retreat space will only be for attendees, sorry. It would be too chaotic otherwise.)

---

If you're interested enough to read this far, you should probably `email at cmart dot today` and tell me what you think. Would Works in Progress work for you? If not, what would need to change? Most of it is still subject to change!