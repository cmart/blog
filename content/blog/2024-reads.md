---
title: "Books I Read in 2024"
date: 2025-01-19T20:00:00-07:00
tags: []
draft: false
---

To avoid grade inflation, I use the cmart rating system:

- -2 is quite bad.
- -1 is sorta bad.
- 0 is meh / okay. Borderline worthwhile.
- +1 is pretty good! Clearly worthwhile.
- +2 is _uncommonly_ great.

If your life doesn't suck, most of your experiences will score between -1 and +1. You hope to avoid -2 things altogether. +2 is an honor to bestow sparingly.

### Tamara Shopsin / [LaserWriter II](https://tamarashopsin.com/lw2/)

**Rating: +2**

Like the protagonist, I began my career in desktop IT support. I wanted to learn everything I could to become _great_ at my job. I discovered the craftsmanship of Apple gear at an impressionable age, back when Macs were so uncommon that many people didn't recognize them. So, I found LWII resonant and nostalgic.

### Jim Benson / [Personal Kanban: Mapping Work | Navigating Life](https://www.personalkanban.com/)

**Rating: -2**

I read this because I wanted clarity on some vagaries in the kanban system, so that I could give it an honest try, both at work and outside it.

The book failed to answer my most important questions, like: How do I de-bikeshed the decision of whether to track something as a single larger task or multiple smaller tasks? Should an item on a kanban board track a unit of effort, or a unit of value delivered to a stakeholder?

I've given up on kanban for self-management. I'm back to keeping lists.

### Kent Beck / [Tidy First?](https://www.oreilly.com/library/view/tidy-first/9781098151232/)

**Rating: +1**

A short, breezy book about the craft of making software. It's $28 for a 100-page paperback, but I'd say worth it if you want to become a better software engineer.

It starts with a list of tidyings -- small changes that make code easier to read, reason about, and maintain. These changes are simple enough to do _while you're in there making other changes_, so they are nearly free wins.

_Tidy First?_ then addresses the title question. Suppose you need to modify or extend the behavior of some existing code. You look at the code and it's kind of a mess. When do you clean up (i.e. refactor) it? Before you make the behavior change, or afterward?

These didn't exactly come from the book, but I generally agree with them:

- Each commit to revision control should make _either_ a behavior / functionality change, _or_ a cleanup / refactor of existing code, but not both. In other words, don't force a reviewer to reason about both a program behavior change and code structure change at the same time. (Future you might be that reviewer!)
- Make the change easy, then make the easy change. Mise en place.

### Jud Brewer / [Unwinding Anxiety](https://drjud.com/book)

**Rating: +1**

Compared to most self-improvement / self-help books, the claims are incrementally more [falsifiable](https://en.wikipedia.org/wiki/Falsifiability) (he's an MD+PhD), and ~~there is slightly less of the "download my app and become a coach" sales pitch~~ [nope, lol](https://unwindinganxiety.com/).

Dr. Jud claims that anxiety is a _compulsive behavior_, just like smoking or over-eating. We fail to recognize this because anxiety happens to occur inside the sufferer's head, rather than as something they _do_ in the physical world. Still, the pattern of trigger -> behavior -> reward is there, even if anxious people wouldn't say that it's rewarding.

If you buy this framing, then a large body of approaches and techniques for addiction recovery becomes relevant for treating chronic anxiety and panic attacks.

Huge if true? I should read this again.

### Marshall B. Rosenberg / [Nonviolent Communication](https://en.wikipedia.org/wiki/Nonviolent_Communication)

**Rating: +1**

Nonviolent Communication is a _very_ different way to interact with people than we tend to by default ("violent communication").

In NVC, the thing that matters most is the emotional needs of your conversation partner. NVC is the process of uncovering and empathizing with someone's feelings and needs, ideally taking turns sharing and receiving.

NVC values facts about the outside world only to the extent that people attribute feelings to them. So, if you're trying to work through some unresolved tension or disagreement that seems to defy resolution on its objective merits, then NVC might be great. It might not be the right tool if your goal is to build a shared understanding of reality in a way that sets feelings aside.

I grew up with the unhelpful myths that the world doesn't care about people's feelings, and that to some extent, feelings aren't even 'real'. So, I've had some difficulty applying NVC in earnest. But there are certainly times when I should.

If you want a flavor of NVC without reading the book, maybe flip through [ANVC Cartoons](https://anvc.svenhartenstein.de/en/1/), or look up some old MBR [hand puppet videos](https://www.youtube.com/watch?v=Xov5z_GJ9Zs).

### Edmond Lau / [The Effective Engineer](https://www.effectiveengineer.com)

**Rating: +1**

This felt like a well-organized collection of guidance and mentorings from many different places. It's most relevant for technologists, but there is something in here for anyone who wants to do a great job at their job.

My nagging concern is that becoming an effective _individual_ might optimize for the wrong thing. Effective _teams_ and _organizations_ seem more enduring, and more able to build great things, than any one individual is. [Google gets this](https://abseil.io/resources/swe-book/html/toc.html0) -- their engineering culture optimizes for team or project velocity, not individual developer velocity. So, when does the Vulcan proverb apply? [^vulcan]

[^vulcan]: "The needs of the many outweigh the needs of the few or the one."

### Freddie deBoer / [The Cult of Smart](https://us.macmillan.com/books/9781250224491/thecultofsmart/)

**Rating: -1**

_The Cult of Smart_ argues that it's bad to have a culture that valorizes intelligence, because many people are not especially smart, and they won't do very well if our society expects above-average intelligence as a prerequisite for a successful career. So, a more humane culture (and education system) should not valorize intelligence as much as ours does. So far, so good.

Then we get to deBoer's call to action: basically, implement the [Democratic Socliaists of America](https://en.wikipedia.org/wiki/Democratic_Socialists_of_America) policy program. Maybe, but how? Whatever you think about socialism, people have tried to champion it here for a century, and electorates aren't that sympathetic to it right now. So, _The Cult of Smart_ leaves me wondering how to draw the rest of the owl.

### [Asterisk Magazine](https://asteriskmag.com)

**Rating: +1**

San Francisco Bay area optimism, packaged for normies and sometimes critiqued. Great articles by some great authors.

Yes, it's not a book, it's a free online magazine. You can order a [print subscription](https://store.asteriskmag.com/), though.

### Dan Davies / [The Unaccountability Machine](https://press.uchicago.edu/ucp/books/book/chicago/U/bo252799883.html)

**Rating: 0**

The deep dive into cybernetics could not hold my attention, but the most useful concept from this book is the _accountability sink_.

Organizations create accountability sinks so that they can systematically disappoint people in a way that no particular member of the organization actually feels responsible (or can be held responsible) for disappointing anyone.

When you learn about this, you'll notice accountability sinks everywhere.

### JL Collins / [Pathfinders](https://harriman.house/pathfinders-sign-up)

**Rating: +1**

(I probably should have read _The Simple Path to Wealth_ by the same author, instead.)

JL Collins is one of several people sharing recipes for retiring in your 40s. This recipe is approximately:

- Become more thrifty (save at least half of your income, much more if you can).
- Invest most/all your savings in low-cost index funds that buy the total US stock market (e.g. the S&P 500).
- Do not sell during a recession. Try to avoid trading often (or even looking at your portfolio).
- Let compound returns go brrr until you're a millionaire, which happens in a much shorter number of years than you may expect.

This advice holds up _if_ the US stock market continues the long-term ~8% annual returns that it's had for the past century. The future might look like the past, or it might not!

I found Mad Fientist's [blog](https://www.madfientist.com/articles/) (and [podcast](https://www.madfientist.com/podcast/)) adds a lot of useful nuance about _off-label_ uses of tax-advantaged retirement accounts toward this goal.

### Burton Malkiel / [A Random Walk Down Wall Street](https://en.wikipedia.org/wiki/A_Random_Walk_Down_Wall_Street)

**Rating: 0**

I learned a lot of interesting trivia from this book. It filled some gaps in my understanding of what the stock market even _is_, and how it works. It has a lot of background on how investment bubbles work, and the history of strategies for choosing stocks to invest in.

But it's probably 10x more than you need to know, if you're just going to invest in index funds.

### Cory Doctorow / [Red Team Blues](https://us.macmillan.com/books/9781250865847/redteamblues/)

**Rating: +1**

Fun and fast present-day detective story with cypherpunk themes. Almost self-consciously tech-aware and "current issues"-aware.

### Ursula Leguin / [The Lathe of Heaven](https://en.wikipedia.org/wiki/The_Lathe_of_Heaven)

**Rating: +2**

Great sci-fi. I loved this book. I also love that it's set in Portland.

I won't spoil, but I ended up with an alternative, real-world-plausible explanation for the core mechanism that drives the plot?

### Chris Voss / [Never Split the Difference](https://www.blackswanltd.com/never-split-the-difference)

**Rating: +1**

An FBI hostage negotiator explains his negotiation strategies.

I didn't seek out this book, but my partner's stepdad basically handed it to me. I _had_ to read it, then my partner had to read it.

I happened to read this after Nonviolent Communication. That was the correct order, because it allowed me to see this book for what it is: _how to wield NVC as a tool to get what you want_.

Please only use this power for good.
