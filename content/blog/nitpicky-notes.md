---
title: "Nitpicky Notes Part 1"
date: 2024-09-15T16:30:00-07:00
tags: ["note-taking", "joplin"]
draft: false
---

A friend showed me [Simple, Non-Commercial, Open Source Notes](https://www.youtube.com/watch?app=desktop&v=XRpHIa-2XCE). (Warning, this video will berate you for 30 minutes.) My values are different from the author's, but I appreciate their methodical(??) approach to articulate their needs and explore the solution space of digital note-taking systems. Here, I do some of the same.

![The image depicts a range of educational and productivity apps; Readwise, Anki, Quizlet, Apple Notes; alongside an IQ distribution chart, juxtaposing two characters pondering their position within the cognitive spectrum and suggesting themes of self-awareness, identity, and the role of technology in personal growth.](../img/nitpicky-notes/apple-notes-meme.jpg)

### Composing and Editing

I write in Markdown without thinking about it. Even with a pen and paper, I'll use Markdown syntax. (You'll ask "Which dialect?" and the answer is "approximately [CommonMark](https://commonmark.org/) plus tables and footnotes".)

I want a fixed-width, plaintext editing view, even when writing prose. It's fine if the editor applies syntax highlighting to the text, but I cannot abide a WYSIWYG / rich-text UX that changes character spacing or hides my markup.

GUI or TUI? Either can be fine, but I appreciate the ability to use an external editor on occasion.

### Organizing and Browsing

The following serves 85% of my organization needs:

- The title of each note begins with its ISO 8601-formatted creation date. (I don't need help typing it.)
- The list of notes is default-sorted by modification date, most recent first, like a messaging app.
- A good full-text search UX. Type a word, see all notes containing a fuzzy match of the word.

I don't want to organize my notes in a hierarchy or taxonomy, because _I don't want to think the following thoughts._

- "Which notebook does this note go in?"
- "Does it need a new sub-notebook?"
- "What do I do about these categories that don't make sense anymore?"

Sorting and organizing notes feels like fake productivity to me, and it causes decision fatigue. I'd rather just search. Similarly, I don't want to garden a web of links between my notes.

That said, I appreciate a [_tag system_](https://en.wikipedia.org/wiki/Tag_(metadata)) for the occasion that I want to group some notes together.

### Storing and Syncing

It needs to be "local-first" software in the sense of storing the data locally on the device.

I'll use multi-device synchronization _iff_ it supports client-side, trust-no-one encryption using pluggable commodity remote storage (S3 or whatever), but I don't strictly need multi-device sync. Anywhere worth going is worth bringing my laptop to.

I don't need multi-user collaboration. [Other tools](https://apps.sandstorm.io/app/4uda6a9yfxk4v3x2mpxjdn2md9y54tj104091480qjr8h758u4dh) do a good enough job of that. I don't want to worry about ACLs or security for a collection of mostly private notes.

I do appreciate automatic / continuous saving, and _automatic versioning_, mostly for recovery from mistakes. If I accidentally `Ctrl+A Backspace Ctrl+S Ctrl+Q`, I want to keep calm and recover the document from a minute ago. That said, I do _not_ want the friction of a revision control system like Git. I've made thousands of Git commits but I don't want to think about them while taking notes.

Unlike some [other folks](https://github.com/0atman/noboilerplate/blob/main/scripts/29-obsidian.md), I don't care whether the underlying storage format is flat files or something like SQLite. I trust that 40-years-older me (or some post-quantum AI archaeologist) will be able to query a SQLite database. I'm even open to something bleeding-edge like [Automerge](https://automerge.org). I take regular backups of my computer, so if a worst-case software update makes the DB eat itself, I can downgrade and restore from last week or whatever.

In summary, it needs to live up to capital-LF [Local-First](https://www.inkandswitch.com/local-first/) principles 1, 3, 5, 6, and 7. (2 is a nice-to-have; don't need 4.)

### Developer Incentives and Licensing

Younger cmart would have said "my notes app needs to be community software project developed and maintained by volunteers". Then I became an open-source project maintainer. I still believe this can result in great software, but it's not the only path (or necessarily the best one).

I would like the developer(s) to have:
- Ongoing conversations with a bunch of thoughtful users.
- Enough vision and taste to say No to bloaty features. "Go write a plugin" can be a good compromise.
- A high bar of quality for accepting external contributions.
- Enough spare cycles (and not too burned out) to engage with bug reports and keep the project relevant. It's fine if response time is slow.

"Non-commercial software" is _not_ a requirement. I am cool with an indie developer making a living from my note-taking software. If the developer charges money, yes, fine. Even if it's free, [yes, I will donate to your Liberapay or whatever](https://cmart.blog/cmart-philanthropy/).

I am personally okay with a license that is not open-source as zealots would define it, as long as the project _did not get there via [rug-pull](https://openpath.chadwhitacre.com/2024/relicensing-and-rug-pulls/)_ from a community built around a different license. [Source-first](https://sourcefirst.com/) is fine. [Delayed open source](https://opensource.org/delayed-open-source-publication) is fine. I'm ideologically agnostic here. I welcome people trying new things.

But we _must_ be able to look at how it's built, less because I want to read the source for fun, more because it's important that anyone _can_ read the source (and build from source). It eases troubleshooting and customization, it raises the ceiling of security, it helps keep everyone honest. I basically agree that people are entitled to know exactly what their computers are doing, and (re-)program their devices to do as they wish.

So yes, this rules out [Obsidian](https://obsidian.md). 👋

I don't want anything with telemetry enabled by default, or any advertising, or VC SaaS incentives at play. (I don't want an [incredible journey](https://ourincrediblejourney.tumblr.com/).) This rules out _many_ things.

## What I use now

On new year's day 2020 I started using [Joplin](https://joplinapp.org). It's been almost 5 years!

## How do I use it?

A few ways! At the beginning of each workday, I make a section like this at the top of a month-specific note.

```
## 2024-09-12

Appts:
- 08:30 Lorem meeting
- 11:30 meeting with Ipsum

To do:
- [x] put dolor on sit amet calendar
- [ ] re-review foo's MR
- [x] make a bar baz
  - Created https://gitlab.com/cmart/qux/-/issues/42
- [x] reproduce and fix Quux's issue with thing
  - created https://example.thing

Notes:
(more words here, catchall and scratch space.)
```

If I start writing a lot about something in the catchall space, I'll move it to its own titled note. I'll also make dedicated notes for learning about a specific topic, important meetings, and for writing posts like this one.

I have 903 total notes going back ~10 years. (I also have much older files that are _not_ worth importing to the note-taking system.)

## What I like about Joplin

Import and export. It happily soaked up my folders of Markdown files _and_ an Evernote export file.

Joplin's plaintext editor. It's pretty good! Sometimes I'd rather edit with [Micro](https://github.com/zyedidia/micro), and I can toggle "external editing" to use that instead.

The searching and (non-)organizing UX. I can put all my notes in a single notebook called "none", find stuff by searching, and use the tag system when I feel like it.

It seems like a healthy software project with healthy long-term incentives. They [made it into Google Summer of Code](https://summerofcode.withgoogle.com/programs/2022/organizations/joplin) (something my own project was rejected from a few times).

## What I don't like about Joplin

When you toggle between viewing different notes, it still [doesn't remember your viewport scroll position or cursor position](https://github.com/laurent22/joplin/issues/5708). Switching to a different note _always_ brings you to the top of the document. This is disorienting when cross-referencing multiple notes.

Also on cross-referencing, the desktop client won't natively display multiple notes at a time. This requires a janky tabs plugin.

The Android app's synchronization [breaks a lot](https://github.com/laurent22/joplin/issues/8402) for me. Even when it nominally works, the sync conflict handling is doodoo. If I want to edit a note on one device and then continue editing it on another device, I generally experience one or more of:

- The note is out-of-date (or doesn't exist) on the second device.
- I need to push the "Synchronize" buttons on both devices in the right order.
- Some of my edits silently end up in the sync conflicts folder.

Maybe I'm doing it wrong, but it's not a reliable experience.

Joplin's automatic versioning feature [has broken for me](https://github.com/laurent22/joplin/issues/5531) in the past, and feels a bit under-baked. (The Android app only lets me keep note history for up to 730 days, yet the desktop Linux app is currently set to 9999 days?)

The Android app has an under-polished drawing feature, which feels like the beginning of feature bloat, though I guess it's harmless.

## What next?

¯\\_(ツ)_/¯ to be continued!
